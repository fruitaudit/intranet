<?php

namespace AppBundle\Datatables;

use AppBundle\Entity\User;
use Exception;
use Sg\DatatablesBundle\Datatable\AbstractDatatable;
use Sg\DatatablesBundle\Datatable\Column\ActionColumn;
use Sg\DatatablesBundle\Datatable\Column\BooleanColumn;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\DateTimeColumn;
use Sg\DatatablesBundle\Datatable\Editable\SelectEditable;
use Sg\DatatablesBundle\Datatable\Filter\DateRangeFilter;
use Sg\DatatablesBundle\Datatable\Filter\SelectFilter;
use Sg\DatatablesBundle\Datatable\Filter\TextFilter;
use Sg\DatatablesBundle\Datatable\Style;

/**
 * Class UserDatatable
 *
 * @package AppBundle\Datatables
 */
class UserDatatable extends AbstractDatatable
{
    /**
     * {@inheritdoc}
     * @throws Exception
     */
    public function buildDatatable(array $options = array()): void
    {
        $actionsDatatable = [];
        $actionsDatatable[] = array(
            'route' => 'app_user_show',
            'route_parameters' => array(
                'id' => 'id',
            ),
            'label' => 'Ver',
            'attributes' => array(
                'rel' => 'tooltip',
                'title' => 'Ver',
                'class' => 'btn btn-default btn-xs',
                'role' => 'button',
            ),
        );

        if (isset($options['isGranted']) && true === $options['isGranted']) {
            $actionsDatatable[] = array(
                'route' => 'app_user_edit',
                'route_parameters' => array(
                    'id' => 'id',
                ),
                'label' => 'Editar',
                'attributes' => array(
                    'rel' => 'tooltip',
                    'title' => 'Editar',
                    'class' => 'btn btn-primary btn-xs',
                    'role' => 'button',
                ),
            );
        }

        $this->language->set(
            array(
                'cdn_language_by_locale' => true
                //'language' => 'de'
            )
        );

        $this->ajax->set(
            array(
                'url' => $this->router->generate('app_user_ajax_update'),
                'method' => 'POST',
                'pipeline' => 0,
            )
        );

        $this->options->set(
            array(
                'order_cells_top' => true,
                'display_start' => 0,
                'length_menu' => array(10, 25),
                'order_classes' => true,
                'order' => array(array(1, 'asc')),
                'order_multi' => true,
                'page_length' => 10,
                'scroll_collapse' => false,
                'search_delay' => 0,
                'state_duration' => 7200,
                'classes' => Style::BOOTSTRAP_3_STYLE.' table-hover table-condensed',
                'individual_filtering' => true,
                'individual_filtering_position' => 'head',
            )
        );

        $this->features->set(
            array(
                'auto_width' => false,
                'defer_render' => false,
                'info' => true,
                'length_change' => true,
                'ordering' => true,
                'paging' => true,
                'processing' => true,
                'scroll_x' => false,
                'scroll_y' => null,
                'searching' => true,
                'state_save' => true,
            )
        );

        $this->extensions->set(
            array(
                'responsive' => true,
            )
        );

        $this->columnBuilder
            ->add(
                'id',
                Column::class,
                array(
                    'title' => "<div class='datatable-th-actions text-center'>Id</div>",
                    'searchable' => false,
                    'orderable' => true,
                    'class_name' => 'text-right',
                )
            )
            ->add(
                'name',
                Column::class,
                array(
                    'title' => "<div class='datatable-th-actions text-center'>Nombre</div>",
                    'searchable' => true,
                    'orderable' => true,
                    'filter' => array(
                        TextFilter::class,
                        array(
                            'cancel_button' => false,
                            'placeholder' => false,
                        ),
                    ),
                )
            )
            ->add(
                'provincia',
                Column::class,
                array(
                    'title' => "<div class='datatable-th-actions text-center'>Provincia</div>",
                    'orderable' => true,
                    'filter' => array(
                        TextFilter::class,
                        array(
                            'cancel_button' => false,
                            'placeholder' => false,
                        ),
                    ),
                )
            )
            ->add(
                'email',
                Column::class,
                array(
                    'title' => "<div class='datatable-th-actions text-center'>Email</div>",
                    'searchable' => true,
                    'orderable' => true,
                    'filter' => array(
                        TextFilter::class,
                        array(
                            'cancel_button' => false,
                            'placeholder' => false,
                        ),
                    ),
                )
            )
            ->add(
                'tlf',
                Column::class,
                array(
                    'title' => "<div class='datatable-th-actions text-center'>Teléfono</div>",
                    'searchable' => true,
                    'orderable' => true,
                    'filter' => array(
                        TextFilter::class,
                        array(
                            'cancel_button' => false,
                            'placeholder' => false,
                        ),
                    ),
                )
            )
            ->add(
                'contacto',
                Column::class,
                array(
                    'title' => "<div class='datatable-th-actions text-center'>Contacto</div>",
                    'searchable' => true,
                    'orderable' => true,
                    'filter' => array(
                        TextFilter::class,
                        array(
                            'cancel_button' => false,
                            'placeholder' => false,
                        ),
                    ),
                )
            )
            ->add(
                'active',
                BooleanColumn::class,
                [
                    'title' => "<div class='datatable-th-actions text-center'>Activo</div>",
                    'orderable' => true,
                    'true_label' => $this->translator->trans('label.yes'),
                    'false_label' => $this->translator->trans('label.no'),
                    'default_content' => $this->translator->trans('label.default_value'),
                    'filter' => [
                        SelectFilter::class,
                        [
                            'classes' => 'text-center',
                            'search_type' => 'eq',
                            'multiple' => false,
                            'select_options' => [
                                '' => $this->translator->trans('label.all'),
                                '1' => $this->translator->trans('label.yes'),
                                '0' => $this->translator->trans('label.no'),
                            ],
                            'cancel_button' => false,
                        ],
                    ],
                    'editable' => [
                        SelectEditable::class,
                        [
                            'source' => [
                                ['value' => 1, 'text' => $this->translator->trans('label.yes')],
                                ['value' => 0, 'text' => $this->translator->trans('label.no')],
                            ],
                            'mode' => 'inline',
                        ],
                    ],
                ]
            )
            ->add(
                'fechaExp',
                DateTimeColumn::class,
                array(
                    'title' => "<div class='datatable-th-actions text-center'>Expira</div>",
                    'searchable' => true,
                    'orderable' => true,
                    'default_content' => '---',
                    'date_format' => 'L',
                    'filter' => array(
                        DateRangeFilter::class,
                        array(
                            'cancel_button' => false,
                            'placeholder' => false,
                        ),
                    ),
                    'timeago' => true,
                )
            )
            ->add(
                'lastAccess',
                DateTimeColumn::class,
                array(
                    'title' => "<div class='datatable-th-actions text-center'>Último Acceso</div>",
                    'searchable' => true,
                    'orderable' => true,
                    'default_content' => '---',
                    'date_format' => 'L',
                    'filter' => array(
                        DateRangeFilter::class,
                        array(
                            'cancel_button' => false,
                            'placeholder' => false,
                        ),
                    ),
                    'timeago' => true,
                )
            )
            ->add(
                'role.name',
                Column::class,
                array(
                    'title' => "<div class='datatable-th-actions text-center'>Rol</div>",
                    'default_content' => '---',
                    'searchable' => true,
                    'orderable' => true,
                    'filter' => array(
                        TextFilter::class,
                        array(
                            'cancel_button' => false,
                            'placeholder' => false,
                        ),
                    ),
                )
            )
            ->add(
                null,
                ActionColumn::class,
                array(
                    'title' => "<div class='datatable-th-actions text-center'>Acciones</div>",
                    'actions' => $actionsDatatable,
                )
            );
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return User::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'user_datatable';
    }
}
