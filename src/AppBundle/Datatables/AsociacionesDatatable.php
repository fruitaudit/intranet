<?php

namespace AppBundle\Datatables;

use AppBundle\Entity\Asociaciones;
use Exception;
use Sg\DatatablesBundle\Datatable\AbstractDatatable;
use Sg\DatatablesBundle\Datatable\Column\ActionColumn;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Filter\TextFilter;
use Sg\DatatablesBundle\Datatable\Style;

/**
 * Class AsociacionesDatatable
 *
 * @package AppBundle\Datatables
 */
class AsociacionesDatatable extends AbstractDatatable
{
    /**
     * {@inheritdoc}
     * @throws Exception
     */
    public function buildDatatable(array $options = array()): void
    {
        $actionsDatatable = [];
        $actionsDatatable[] = array(
            'route' => 'admin_asociaciones_show',
            'route_parameters' => array(
                'id' => 'id',
            ),
            'label' => 'Ver',
            'attributes' => array(
                'rel' => 'tooltip',
                'title' => 'Ver',
                'class' => 'btn btn-default btn-xs',
                'role' => 'button',
            ),
        );

        if (isset($options['isGranted']) && true === $options['isGranted']) {
            $actionsDatatable[] = array(
                'route' => 'admin_asociaciones_edit',
                'route_parameters' => array(
                    'id' => 'id',
                ),
                'label' => 'Editar',
                'attributes' => array(
                    'rel' => 'tooltip',
                    'title' => 'Editar',
                    'class' => 'btn btn-warning btn-xs',
                    'role' => 'button',
                ),
            );
        }

        $this->language->set(
            array(
                'cdn_language_by_locale' => true
                //'language' => 'de'
            )
        );

        $this->ajax->set(
            array(
                'url' => $this->router->generate('admin_asociaciones_ajax_update'),
                'method' => 'POST',
                'pipeline' => 0,
            )
        );

        $this->options->set(
            array(
                'order_cells_top' => true,
                'display_start' => 0,
                'length_menu' => array(10, 25),
                'order_classes' => true,
                'order' => array(array(1, 'asc')),
                'order_multi' => true,
                'page_length' => 10,
                'scroll_collapse' => false,
                'search_delay' => 0,
                'state_duration' => 7200,
                'classes' => Style::BOOTSTRAP_3_STYLE.' table-hover table-condensed',
                'individual_filtering' => true,
                'individual_filtering_position' => 'head',
            )
        );

        $this->features->set(
            array(
                'auto_width' => false,
                'defer_render' => false,
                'info' => true,
                'length_change' => true,
                'ordering' => true,
                'paging' => true,
                'processing' => true,
                'scroll_x' => false,
                'scroll_y' => null,
                'searching' => true,
                'state_save' => true,
            )
        );

        $this->extensions->set(
            array(
                'responsive' => true,
            )
        );

        $this->columnBuilder
            ->add(
                'id',
                Column::class,
                array(
                    'title' => "<div class='datatable-th-actions text-center'>Id</div>",
                    'searchable' => false,
                    'orderable' => true,
                    'class_name' => 'text-right',
                )
            )
            ->add(
                'nombre',
                Column::class,
                array(
                    'title' => "<div class='datatable-th-actions text-center'>Nombre</div>",
                    'searchable' => true,
                    'orderable' => true,
                    'filter' => array(
                        TextFilter::class,
                        array(
                            'cancel_button' => false,
                            'placeholder' => false,
                        ),
                    ),
                )
            )
            ->add(
                null,
                ActionColumn::class,
                array(
                    'title' => "<div class='datatable-th-actions text-center'>Acciones</div>",
                    'actions' => $actionsDatatable,
                )
            );
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return Asociaciones::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'asociaciones_datatable';
    }
}
