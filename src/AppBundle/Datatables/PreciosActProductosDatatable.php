<?php

namespace AppBundle\Datatables;

use AppBundle\Entity\PreciosActProd;
use Exception;
use Sg\DatatablesBundle\Datatable\AbstractDatatable;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\DateTimeColumn;
use Sg\DatatablesBundle\Datatable\Filter\TextFilter;
use Sg\DatatablesBundle\Datatable\Style;

/**
 * Class PreciosActProductosDatatable
 *
 * @package AppBundle\Datatables
 */
class PreciosActProductosDatatable extends AbstractDatatable
{
    /**
     * {@inheritdoc}
     * @throws Exception
     */
    public function buildDatatable(array $options = array()): void
    {
        $this->language->set(
            array(
                'cdn_language_by_locale' => true
                //'language' => 'de'
            )
        );

        $this->ajax->set(
            array(
                'url' => $this->router->generate(
                    'estadisticas_precios_producto_ajax_update',
                    ['id' => $options['routeId']]
                ),
                'method' => 'POST',
                'pipeline' => 0,
            )
        );

        $this->options->set(
            array(
                'order_cells_top' => true,
                'display_start' => 0,
                'length_menu' => array(10, 25),
                'order_classes' => true,
                'order' => array(array(1, 'asc')),
                'order_multi' => true,
                'page_length' => 10,
                'scroll_collapse' => false,
                'search_delay' => 0,
                'state_duration' => 7200,
                'classes' => Style::BOOTSTRAP_3_STYLE.' table-hover table-condensed',
                'individual_filtering' => true,
                'individual_filtering_position' => 'head',
            )
        );

        $this->features->set(
            array(
                'auto_width' => false,
                'defer_render' => false,
                'info' => true,
                'length_change' => true,
                'ordering' => true,
                'paging' => true,
                'processing' => true,
                'scroll_x' => false,
                'scroll_y' => null,
                'searching' => true,
                'state_save' => true,
            )
        );

        $this->extensions->set(
            array(
                'responsive' => true,
            )
        );

        $this->columnBuilder
            ->add(
                'supermercado.nombre',
                Column::class,
                array(
                    'title' => "<div class='datatable-th-actions text-center'>Supermercado</div>",
                    'searchable' => true,
                    'orderable' => true,
                    'filter' => array(
                        TextFilter::class,
                        array(
                            'cancel_button' => false,
                            'placeholder' => false,
                        ),
                    ),
                )
            )
            ->add(
                'envase',
                Column::class,
                array(
                    'title' => "<div class='datatable-th-actions text-center'>Envase</div>",
                    'searchable' => true,
                    'orderable' => true,
                    'filter' => array(
                        TextFilter::class,
                        array(
                            'cancel_button' => false,
                            'placeholder' => false,
                        ),
                    ),
                )
            )
            ->add(
                'origen',
                Column::class,
                array(
                    'title' => "<div class='datatable-th-actions text-center'>Origen</div>",
                    'searchable' => true,
                    'orderable' => true,
                    'filter' => array(
                        TextFilter::class,
                        array(
                            'cancel_button' => false,
                            'placeholder' => false,
                        ),
                    ),
                )
            )
            ->add(
                'fecha',
                DateTimeColumn::class,
                array(
                    'title' => "<div class='datatable-th-actions text-center'>Fecha</div>",
                    'searchable' => true,
                    'orderable' => true,
                    'date_format' => 'L',
                    'filter' => array(
                        TextFilter::class,
                        array(
                            'cancel_button' => false,
                            'placeholder' => false,
                        ),
                    ),
                )
            )
            ->add(
                'precio',
                Column::class,
                array(
                    'title' => "<div class='datatable-th-actions text-center'>Precio</div>",
                    'searchable' => true,
                    'orderable' => true,
                    'filter' => array(
                        TextFilter::class,
                        array(
                            'cancel_button' => false,
                            'placeholder' => false,
                        ),
                    ),
                )
            );
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return PreciosActProd::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'precios_productos_datatable';
    }
}
