<?php

namespace AppBundle\Controller;

use AppBundle\Datatables\PreciosSupermercadoDatatable;
use AppBundle\Entity\PreciosSupermercado;
use AppBundle\Form\PreciosSupermercadoType;
use AppBundle\Service\PreciosSupermercadoService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Preciossupermercado controller.
 *
 * @Route("admin/supermercados")
 */
class PreciosSupermercadoController extends Controller
{
    /**
     * @Route("/", name="admin_supermercados", methods={"GET"})
     * @throws Exception
     */
    public function indexAction(): Response
    {
        $datatable = $this->get('sg_datatables.factory')->create(PreciosSupermercadoDatatable::class);
        $datatable->buildDatatable(['isGranted' => $this->isGranted('ROLE_ADMIN')]);

        return $this->render('AppBundle:preciossupermercado:index.html.twig', array('datatable' => $datatable));
    }

    /**
     * @Route("/ajax-update/", name="admin_supermercados_ajax_update", methods={"POST"})
     * @return JsonResponse
     */
    public function ajaxUpdate(): ?JsonResponse
    {
        try {
            $datatable = $this->get('sg_datatables.factory')->create(PreciosSupermercadoDatatable::class);
            $datatable->buildDatatable(['isGranted' => $this->isGranted('ROLE_ADMIN')]);
            $responseService = $this->get('sg_datatables.response');
            $responseService->setDatatable($datatable);
            $responseService->getDatatableQueryBuilder();

            return $responseService->getResponse();
        } catch (\Exception $exception) {
            return new JsonResponse(array('error' => 'ajax-update'));
        }
    }

    /**
     * @Route("/", name="admin_supermercados_create", methods={"POST"})
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function createAction(Request $request)
    {
        $entity = new PreciosSupermercado();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $preciosSupermercadoService = $this->get(PreciosSupermercadoService::class);
            $preciosSupermercadoService->uploadImage($entity);
            $preciosSupermercadoService->save($entity);

            $this->addFlash('success', 'PreciosSupermercado guardado correctamente');

            return $this->redirectToRoute('admin_supermercados_show', array('id' => $entity->getId()));
        }

        return $this->render(
            'AppBundle:preciossupermercado:new.html.twig',
            array(
                'entity' => $entity,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * @param PreciosSupermercado $entity
     *
     * @return FormInterface
     */
    private function createCreateForm(PreciosSupermercado $entity): FormInterface
    {
        $form = $this->createForm(
            PreciosSupermercadoType::class,
            $entity,
            array(
                'action' => $this->generateUrl('admin_supermercados_create'),
                'method' => 'POST',
            )
        );

        return $form;
    }

    /**
     * @Route("/new", name="admin_supermercados_new", methods={"GET"})
     */
    public function newAction(): Response
    {
        $entity = new PreciosSupermercado();
        $form = $this->createCreateForm($entity);

        return $this->render(
            'AppBundle:preciossupermercado:new.html.twig',
            array(
                'entity' => $entity,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * @Route("/{id}", name="admin_supermercados_show", methods={"GET"})
     * @param $id
     * @return Response
     */
    public function showAction($id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $entity = $entityManager->getRepository('AppBundle:PreciosSupermercado')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Entity not found PreciosSupermercado.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render(
            'AppBundle:preciossupermercado:show.html.twig',
            array(
                'entity' => $entity,
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * @Route("/{id}/edit", name="admin_supermercados_edit", methods={"GET"})
     * @param $id
     * @return Response
     */
    public function editAction($id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $entity = $entityManager->getRepository('AppBundle:PreciosSupermercado')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Entity not found PreciosSupermercado.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render(
            'AppBundle:preciossupermercado:edit.html.twig',
            array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * @param PreciosSupermercado $entity
     *
     * @return FormInterface
     */
    private function createEditForm(PreciosSupermercado $entity): FormInterface
    {
        $form = $this->createForm(
            PreciosSupermercadoType::class,
            $entity,
            array(
                'action' => $this->generateUrl('admin_supermercados_update', array('id' => $entity->getId())),
                'method' => 'PUT',
            )
        );

        return $form;
    }

    /**
     * @Route("/{id}", name="admin_supermercados_update", methods={"PUT"})
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function updateAction(Request $request, $id): RedirectResponse
    {
        $entityManager = $this->getDoctrine()->getManager();

        $entity = $entityManager->getRepository('AppBundle:PreciosSupermercado')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Entity not found PreciosSupermercado.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $preciosSupermercadoService = $this->get(PreciosSupermercadoService::class);
            $preciosSupermercadoService->uploadImage($entity);
            $preciosSupermercadoService->save($entity);

            $this->addFlash('success', 'PreciosSupermercado guardado correctamente');

            return $this->redirectToRoute('admin_supermercados_show', array('id' => $id));
        }

        return $this->redirectToRoute('admin_supermercados_edit', array('id' => $id));
    }

    /**
     * @Route("/{id}", name="admin_supermercados_delete", methods={"DELETE"})
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, $id): RedirectResponse
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entity = $entityManager->getRepository('AppBundle:PreciosSupermercado')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Entity not found PreciosSupermercado.');
            }

            $entityManager->remove($entity);
            $entityManager->flush();

            $this->addFlash('success', 'PreciosSupermercado eliminado correctamente');
        }

        return $this->redirectToRoute('admin_supermercados');
    }

    /**
     * @param mixed $id The entity id
     *
     * @return FormInterface
     */
    private function createDeleteForm($id): FormInterface
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_supermercados_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', SubmitType::class, array('label' => 'Delete', 'attr' => array('class' => 'btn-danger')))
            ->getForm();
    }
}
