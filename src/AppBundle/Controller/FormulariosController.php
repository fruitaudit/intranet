<?php

namespace AppBundle\Controller;

use AppBundle\Datatables\FormulariosDatatable;
use AppBundle\Entity\Formularios;
use AppBundle\Form\FormulariosType;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Formulario controller.
 *
 * @Route("formularios")
 */
class FormulariosController extends Controller
{
    /**
     * @Route("/", name="formularios", methods={"GET"})
     * @throws Exception
     */
    public function indexAction(): Response
    {
        $datatable = $this->get('sg_datatables.factory')->create(FormulariosDatatable::class);
        $datatable->buildDatatable(['isGranted' => $this->isGranted('ROLE_ADMIN')]);

        return $this->render('AppBundle:formularios:index.html.twig', array('datatable' => $datatable));
    }

    /**
     * @Route("/ajax-update/", name="formularios_ajax_update", methods={"POST"})
     * @return JsonResponse
     */
    public function ajaxUpdate(): ?JsonResponse
    {
        try {
            $datatable = $this->get('sg_datatables.factory')->create(FormulariosDatatable::class);
            $datatable->buildDatatable(['isGranted' => $this->isGranted('ROLE_ADMIN')]);
            $responseService = $this->get('sg_datatables.response');
            $responseService->setDatatable($datatable);
            $responseService->getDatatableQueryBuilder();

            return $responseService->getResponse();
        } catch (Exception $exception) {
            return new JsonResponse(array('error' => 'ajax-update'));
        }
    }

    /**
     * @Route("/", name="formularios_create", methods={"POST"})
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function createAction(Request $request)
    {
        $entity = new Formularios();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($entity);
            $entityManager->flush();

            $this->addFlash('success', 'Formularios guardado correctamente');

            return $this->redirectToRoute('formularios_show', array('id' => $entity->getId()));
        }

        return $this->render(
            'AppBundle:formularios:new.html.twig',
            array(
                'entity' => $entity,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * @param Formularios $entity
     *
     * @return FormInterface
     */
    private function createCreateForm(Formularios $entity): FormInterface
    {
        $form = $this->createForm(
            FormulariosType::class,
            $entity,
            array(
                'action' => $this->generateUrl('formularios_create'),
                'method' => 'POST',
            )
        );

        return $form;
    }

    /**
     * @Route("/new", name="formularios_new", methods={"GET"})
     */
    public function newAction(): Response
    {
        $entity = new Formularios();
        $form = $this->createCreateForm($entity);

        return $this->render(
            'AppBundle:formularios:new.html.twig',
            array(
                'entity' => $entity,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * @Route("/{id}", name="formularios_show", methods={"GET"})
     * @param $id
     * @return Response
     */
    public function showAction($id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $entity = $entityManager->getRepository('AppBundle:Formularios')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Entity not found Formularios.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render(
            'AppBundle:formularios:show.html.twig',
            array(
                'entity' => $entity,
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * @Route("/{id}/edit", name="formularios_edit", methods={"GET"})
     * @param $id
     * @return Response
     */
    public function editAction($id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $entity = $entityManager->getRepository('AppBundle:Formularios')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Entity not found Formularios.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render(
            'AppBundle:formularios:edit.html.twig',
            array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * @param Formularios $entity
     *
     * @return FormInterface
     */
    private function createEditForm(Formularios $entity): FormInterface
    {
        $form = $this->createForm(
            FormulariosType::class,
            $entity,
            array(
                'action' => $this->generateUrl('formularios_update', array('id' => $entity->getId())),
                'method' => 'PUT',
            )
        );

        return $form;
    }

    /**
     * @Route("/{id}", name="formularios_update", methods={"PUT"})
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function updateAction(Request $request, $id): RedirectResponse
    {
        $entityManager = $this->getDoctrine()->getManager();

        $entity = $entityManager->getRepository('AppBundle:Formularios')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Entity not found Formularios.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $entityManager->flush();
            $this->addFlash('success', 'Formularios guardado correctamente');

            return $this->redirectToRoute('formularios_show', array('id' => $id));
        }

        return $this->redirectToRoute('formularios_edit', array('id' => $id));
    }

    /**
     * @Route("/{id}", name="formularios_delete", methods={"DELETE"})
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, $id): RedirectResponse
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entity = $entityManager->getRepository('AppBundle:Formularios')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Entity not found Formularios.');
            }

            $entityManager->remove($entity);
            $entityManager->flush();

            $this->addFlash('success', 'Formularios eliminado correctamente');
        }

        return $this->redirectToRoute('formularios');
    }

    /**
     * @param mixed $id The entity id
     *
     * @return FormInterface
     */
    private function createDeleteForm($id): FormInterface
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('formularios_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', SubmitType::class, array('label' => 'Delete', 'attr' => array('class' => 'btn-danger')))
            ->getForm();
    }
}
