<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Directorio;
use AppBundle\Form\DirectorioSearchType;
use AppBundle\Form\DirectorioType;
use AppBundle\Model\DirectorioSearch;
use AppBundle\Service\DirectorioService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Directorio controller.
 *
 * @Route("directorio")
 */
class DirectorioController extends Controller
{
    /**
     * @Route("/", name="directorio", methods={"GET"})
     * @throws Exception
     */
    public function indexAction(): Response
    {
        $directorios = $this->get(DirectorioService::class)->findBy(['sucursal' => null], ['nombre' => 'ASC']);

        $searchForm = $this->createSearchForm(new DirectorioSearch());

        return $this->render(
            'AppBundle:directorio:index.html.twig',
            [
                'directorios' => $directorios,
                'searchForm' => $searchForm->createView(),
            ]
        );
    }

    /**
     * @Route("/ajax-search", name="directorio_ajax_search", methods={"POST"})
     * @throws Exception
     */
    public function searchAction(): Response
    {
        $directorios = $this->get(DirectorioService::class)->findBy(['sucursal' => 0], ['nombre' => 'ASC']);

        return $this->render('AppBundle:directorio:index.html.twig', array('directorios' => $directorios));
    }

    /**
     * @Route("/", name="directorio_create", methods={"POST"})
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function createAction(Request $request)
    {
        $entity = new Directorio();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($entity);
            $entityManager->flush();

            $this->addFlash('success', 'Directorio guardado correctamente');

            return $this->redirectToRoute('directorio_show', array('id' => $entity->getId()));
        }

        return $this->render(
            'AppBundle:directorio:new.html.twig',
            array(
                'entity' => $entity,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * @param Directorio $entity
     *
     * @return FormInterface
     */
    private function createCreateForm(Directorio $entity): FormInterface
    {
        $form = $this->createForm(
            DirectorioType::class,
            $entity,
            array(
                'action' => $this->generateUrl('directorio_create'),
                'method' => 'POST',
            )
        );

        return $form;
    }

    /**
     * @Route("/new", name="directorio_new", methods={"GET"})
     */
    public function newAction(): Response
    {
        $entity = new Directorio();
        $form = $this->createCreateForm($entity);

        return $this->render(
            'AppBundle:directorio:new.html.twig',
            array(
                'entity' => $entity,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * @Route("/{id}", name="directorio_show", methods={"GET"})
     * @param $id
     * @return Response
     */
    public function showAction($id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $entity = $entityManager->getRepository('AppBundle:Directorio')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Entity not found Directorio.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render(
            'AppBundle:directorio:show.html.twig',
            array(
                'entity' => $entity,
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * @Route("/{id}/edit", name="directorio_edit", methods={"GET"})
     * @param $id
     * @return Response
     */
    public function editAction($id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $entity = $entityManager->getRepository('AppBundle:Directorio')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Entity not found Directorio.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render(
            'AppBundle:directorio:edit.html.twig',
            array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * @param Directorio $entity
     *
     * @return FormInterface
     */
    private function createEditForm(Directorio $entity): FormInterface
    {
        $form = $this->createForm(
            DirectorioType::class,
            $entity,
            array(
                'action' => $this->generateUrl('directorio_update', array('id' => $entity->getId())),
                'method' => 'PUT',
            )
        );

        return $form;
    }

    /**
     * @Route("/{id}", name="directorio_update", methods={"PUT"})
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function updateAction(Request $request, $id): RedirectResponse
    {
        $entityManager = $this->getDoctrine()->getManager();

        $entity = $entityManager->getRepository('AppBundle:Directorio')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Entity not found Directorio.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $entityManager->flush();
            $this->addFlash('success', 'Directorio guardado correctamente');

            return $this->redirectToRoute('directorio_show', array('id' => $id));
        }

        return $this->redirectToRoute('directorio_edit', array('id' => $id));
    }

    /**
     * @Route("/{id}", name="directorio_delete", methods={"DELETE"})
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, $id): RedirectResponse
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entity = $entityManager->getRepository('AppBundle:Directorio')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Entity not found Directorio.');
            }

            $entityManager->remove($entity);
            $entityManager->flush();

            $this->addFlash('success', 'Directorio eliminado correctamente');
        }

        return $this->redirectToRoute('directorio');
    }

    /**
     * @param mixed $id The entity id
     *
     * @return FormInterface
     */
    private function createDeleteForm($id): FormInterface
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('directorio_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', SubmitType::class, array('label' => 'Delete', 'attr' => array('class' => 'btn-danger')))
            ->getForm();
    }

    /**
     * @param DirectorioSearch $param
     * @return FormInterface
     */
    private function createSearchForm(DirectorioSearch $param): FormInterface
    {
        return $this->createForm(DirectorioSearchType::class);
    }
}
