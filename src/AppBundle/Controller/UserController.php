<?php

namespace AppBundle\Controller;

use AppBundle\Datatables\UserDatatable;
use AppBundle\Entity\User;
use AppBundle\Form\UserEditType;
use AppBundle\Form\UserPasswordType;
use AppBundle\Form\UserType;
use AppBundle\Service\UserService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * User controller.
 *
 * @Route("admin/user")
 */
class UserController extends Controller
{
    /**
     * @Route("/", name="app_user", methods={"GET"})
     * @throws Exception
     */
    public function indexAction(): Response
    {
        $datatable = $this->get('sg_datatables.factory')->create(UserDatatable::class);
        $datatable->buildDatatable(['isGranted' => $this->isGranted('ROLE_ADMIN')]);

        return $this->render('@App/user/index.html.twig', array('datatable' => $datatable));
    }

    /**
     * @Route("/ajax-update/", name="app_user_ajax_update", methods={"POST"})
     * @return JsonResponse
     */
    public function ajaxUpdate(): JsonResponse
    {
        try {
            $datatable = $this->get('sg_datatables.factory')->create(UserDatatable::class);
            $datatable->buildDatatable(['isGranted' => $this->isGranted('ROLE_ADMIN')]);
            $responseService = $this->get('sg_datatables.response');
            $responseService->setDatatable($datatable);
            $responseService->getDatatableQueryBuilder();

            return $responseService->getResponse();
        } catch (Exception $e) {
            return new JsonResponse(array('error' => $e->getMessage()));
        }
    }

    /**
     * @Route("/", name="app_user_create", methods={"POST"})
     *
     * @param Request $request
     * @return Response|RedirectResponse
     */
    public function createAction(Request $request)
    {
        $entity = new User();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $userService = $this->get(UserService::class);
            $userService->encodePassword($entity);
            $userService->uploadAvatar($entity);

            return $this->redirectToRoute('app_user_show', array('id' => $entity->getId()));
        }

        return $this->render(
            '@App/user/new.html.twig',
            array(
                'entity' => $entity,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * @param User $entity
     *
     * @return FormInterface
     */
    private function createCreateForm(User $entity): FormInterface
    {
        $form = $this->createForm(
            UserType::class,
            $entity,
            array(
                'action' => $this->generateUrl('app_user_create'),
                'method' => 'POST',
            )
        );

        return $form;
    }

    /**
     * @Route("/new", name="app_user_new", methods={"GET"})
     */
    public function newAction(): Response
    {
        $entity = new User();
        $form = $this->createCreateForm($entity);

        return $this->render(
            '@App/user/new.html.twig',
            array(
                'entity' => $entity,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * @Route("/{id}", name="app_user_show", methods={"GET"})
     *
     * @param int $id
     * @return Response
     */
    public function showAction($id): Response
    {
        $entity = $this->get(UserService::class)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Entity not found User.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render(
            '@App/user/show.html.twig',
            array(
                'entity' => $entity,
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * @Route("/{id}/edit", name="app_user_edit", methods={"GET"})
     *
     * @param int $id
     * @return Response
     */
    public function editAction($id): Response
    {
        $entity = $this->get(UserService::class)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Entity not found User.');
        }

        $editForm = $this->createEditForm($entity);

        return $this->render(
            '@App/user/edit.html.twig',
            array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
            )
        );
    }

    /**
     * @param User $entity
     *
     * @return FormInterface
     */
    private function createEditForm(User $entity): FormInterface
    {
        $form = $this->createForm(
            UserEditType::class,
            $entity,
            array(
                'action' => $this->generateUrl('app_user_update', array('id' => $entity->getId())),
                'method' => 'PUT',
            )
        );

        return $form;
    }

    /**
     * @Route("/{id}/change-password", name="app_user_change_password")
     *
     * @param Request $request
     * @param $id
     * @return Response|RedirectResponse
     */
    public function changePasswordAction(Request $request, $id)
    {
        $entity = $this->get(UserService::class)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Entity not found User.');
        }

        $editForm = $this->createPasswordForm($entity);

        if (mb_strtolower($request->getMethod()) === 'put') {
            $editForm->handleRequest($request);

            if ($editForm->isValid()) {
                $this->get(UserService::class)->encodePassword($entity);

                $this->addFlash('success', 'Password changed');

                return $this->redirectToRoute('app_user_show', array('id' => $entity->getId()));
            }
        }

        return $this->render(
            '@App/user/edit_password.html.twig',
            array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
            )
        );
    }

    /**
     * @param User $entity
     *
     * @return FormInterface
     */
    private function createPasswordForm(User $entity): FormInterface
    {
        $form = $this->createForm(
            UserPasswordType::class,
            $entity,
            array(
                'action' => $this->generateUrl('app_user_change_password', array('id' => $entity->getId())),
                'method' => 'PUT',
            )
        );

        return $form;
    }

    /**
     * @Route("/{id}", name="app_user_update", methods={"PUT"})
     *
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function updateAction(Request $request, $id): RedirectResponse
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository(User::class)->findOneBy(array('id' => $id));

        if (!$entity) {
            throw $this->createNotFoundException('Entity not found User.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $userService = $this->get(UserService::class);
            $userService->uploadAvatar($entity);

            if (!$entity->isActive()) {
                $lastUserSessionId = $entity->getSession();
                if ($lastUserSessionId) {
                    $sessionFile = $this->getParameter(
                            'session.save_path'
                        ).DIRECTORY_SEPARATOR.'sess_'.$lastUserSessionId;
                    if (is_file($sessionFile)) {
                        unlink($sessionFile);
                    }
                }
            }

            $this->addFlash('success', 'User guardado correctamente');

            return $this->redirectToRoute('app_user_edit', array('id' => $id));
        }

        return $this->redirectToRoute('app_user_show', array('id' => $id));
    }

    /**
     * @Route("/{id}", name="app_user_delete", methods={"DELETE"})
     *
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, $id): RedirectResponse
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->get(UserService::class)->remove($id);
        }

        return $this->redirectToRoute('app_user');
    }

    /**
     * @param mixed $id
     *
     * @return FormInterface
     */
    private function createDeleteForm($id): FormInterface
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('app_user_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add(
                'submit',
                SubmitType::class,
                array('label' => 'label.delete', 'attr' => array('class' => 'btn-danger'))
            )
            ->getForm();
    }
}
