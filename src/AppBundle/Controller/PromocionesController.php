<?php

namespace AppBundle\Controller;

use AppBundle\Datatables\PromocionesDatatable;
use AppBundle\Entity\Promociones;
use AppBundle\Form\PromocionesType;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Promocione controller.
 *
 * @Route("admin/promociones")
 */
class PromocionesController extends Controller
{
    /**
     * @Route("/", name="admin_promociones", methods={"GET"})
     * @throws Exception
     */
    public function indexAction(): Response
    {
        $datatable = $this->get('sg_datatables.factory')->create(PromocionesDatatable::class);
        $datatable->buildDatatable();

        return $this->render('AppBundle:promociones:index.html.twig', array('datatable' => $datatable));
    }

    /**
     * @Route("/ajax-update/", name="admin_promociones_ajax_update", methods={"POST"})
     * @return JsonResponse
     */
    public function ajaxUpdate(): ?JsonResponse
    {
        try {
            $datatable = $this->get('sg_datatables.factory')->create(PromocionesDatatable::class);
            $datatable->buildDatatable();
            $responseService = $this->get('sg_datatables.response');
            $responseService->setDatatable($datatable);
            $responseService->getDatatableQueryBuilder();

            return $responseService->getResponse();
        } catch (Exception $exception) {
            return new JsonResponse(array('error' => 'ajax-update'));
        }
    }

    /**
     * @Route("/", name="admin_promociones_create", methods={"POST"})
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function createAction(Request $request)
    {
        $entity = new Promociones();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($entity);
            $entityManager->flush();

            $this->addFlash('success', 'Promociones guardado correctamente');

            return $this->redirectToRoute('admin_promociones_show', array('id' => $entity->getId()));
        }

        return $this->render(
            'AppBundle:promociones:new.html.twig',
            array(
                'entity' => $entity,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * @param Promociones $entity
     *
     * @return FormInterface
     */
    private function createCreateForm(Promociones $entity): FormInterface
    {
        $form = $this->createForm(
            PromocionesType::class,
            $entity,
            array(
                'action' => $this->generateUrl('admin_promociones_create'),
                'method' => 'POST',
            )
        );

        return $form;
    }

    /**
     * @Route("/new", name="admin_promociones_new", methods={"GET"})
     */
    public function newAction(): Response
    {
        $entity = new Promociones();
        $form = $this->createCreateForm($entity);

        return $this->render(
            'AppBundle:promociones:new.html.twig',
            array(
                'entity' => $entity,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * @Route("/{id}", name="admin_promociones_show", methods={"GET"})
     * @param $id
     * @return Response
     */
    public function showAction($id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $entity = $entityManager->getRepository('AppBundle:Promociones')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Entity not found Promociones.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render(
            'AppBundle:promociones:show.html.twig',
            array(
                'entity' => $entity,
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * @Route("/{id}/edit", name="admin_promociones_edit", methods={"GET"})
     * @param $id
     * @return Response
     */
    public function editAction($id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $entity = $entityManager->getRepository('AppBundle:Promociones')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Entity not found Promociones.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render(
            'AppBundle:promociones:edit.html.twig',
            array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * @param Promociones $entity
     *
     * @return FormInterface
     */
    private function createEditForm(Promociones $entity): FormInterface
    {
        $form = $this->createForm(
            PromocionesType::class,
            $entity,
            array(
                'action' => $this->generateUrl('admin_promociones_update', array('id' => $entity->getId())),
                'method' => 'PUT',
            )
        );

        return $form;
    }

    /**
     * @Route("/{id}", name="admin_promociones_update", methods={"PUT"})
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function updateAction(Request $request, $id): RedirectResponse
    {
        $entityManager = $this->getDoctrine()->getManager();

        $entity = $entityManager->getRepository('AppBundle:Promociones')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Entity not found Promociones.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $entityManager->flush();
            $this->addFlash('success', 'Promociones guardado correctamente');

            return $this->redirectToRoute('admin_promociones_show', array('id' => $id));
        }

        return $this->redirectToRoute('admin_promociones_edit', array('id' => $id));
    }

    /**
     * @Route("/{id}", name="admin_promociones_delete", methods={"DELETE"})
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, $id): RedirectResponse
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entity = $entityManager->getRepository('AppBundle:Promociones')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Entity not found Promociones.');
            }

            $entityManager->remove($entity);
            $entityManager->flush();

            $this->addFlash('success', 'Promociones eliminado correctamente');
        }

        return $this->redirectToRoute('admin_promociones');
    }

    /**
     * @param mixed $id The entity id
     *
     * @return FormInterface
     */
    private function createDeleteForm($id): FormInterface
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_promociones_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', SubmitType::class, array('label' => 'Delete', 'attr' => array('class' => 'btn-danger')))
            ->getForm();
    }
}
