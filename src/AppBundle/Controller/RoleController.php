<?php

namespace AppBundle\Controller;

use AppBundle\Datatables\RoleDatatable;
use AppBundle\Entity\Role;
use AppBundle\Form\RoleType;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RoleController
 *
 * @package AppBundle\Controller
 * @Route("admin/role")
 */
class RoleController extends Controller
{
    /**
     * @Route("/", name="app_role", methods={"GET"})
     * @throws Exception
     */
    public function indexAction(): Response
    {
        $datatable = $this->get('sg_datatables.factory')->create(RoleDatatable::class);
        $datatable->buildDatatable(['isGranted' => $this->isGranted('ROLE_ADMIN')]);

        return $this->render('@App/role/index.html.twig', array('datatable' => $datatable));
    }

    /**
     * @Route("/ajax-update/", name="app_role_ajax_update", methods={"POST"})
     *
     * @return JsonResponse
     */
    public function ajaxUpdate(): JsonResponse
    {
        try {
            $datatable = $this->get('sg_datatables.factory')->create(RoleDatatable::class);
            $datatable->buildDatatable(['isGranted' => $this->isGranted('ROLE_ADMIN')]);
            $responseService = $this->get('sg_datatables.response');
            $responseService->setDatatable($datatable);
            $responseService->getDatatableQueryBuilder();

            return $responseService->getResponse();
        } catch (Exception $exception) {
            return new JsonResponse(array('error' => 'ajax-update'));
        }
    }

    /**
     * @Route("/", name="app_role_create", methods={"POST"})
     *
     * @param Request $request
     * @return Response|RedirectResponse
     */
    public function createAction(Request $request)
    {
        $entity = new Role();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirectToRoute('app_role_show', array('id' => $entity->getId()));
        }

        return $this->render(
            '@App/role/new.html.twig',
            array(
                'entity' => $entity,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * @param Role $entity
     *
     * @return FormInterface
     */
    private function createCreateForm(Role $entity): FormInterface
    {
        $form = $this->createForm(
            RoleType::class,
            $entity,
            array(
                'action' => $this->generateUrl('app_role_create'),
                'method' => 'POST',
            )
        );

        return $form;
    }

    /**
     * @Route("/new", name="app_role_new", methods={"GET"})
     */
    public function newAction(): Response
    {
        $entity = new Role();
        $form = $this->createCreateForm($entity);

        return $this->render(
            '@App/role/new.html.twig',
            array(
                'entity' => $entity,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * @Route("/{id}", name="app_role_show", methods={"GET"})
     *
     * @param $id
     * @return Response
     */
    public function showAction($id): Response
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository(Role::class)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Entity not found Role.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render(
            '@App/role/show.html.twig',
            array(
                'entity' => $entity,
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * @Route("/{id}/edit", name="app_role_edit", methods={"GET"})
     *
     * @param $id
     * @return Response
     */
    public function editAction($id): Response
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository(Role::class)->findOneBy(array('id' => $id));

        if (!$entity) {
            throw $this->createNotFoundException('Entity not found Role.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render(
            '@App/role/edit.html.twig',
            array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * @param Role $entity
     *
     * @return FormInterface
     */
    private function createEditForm(Role $entity): FormInterface
    {
        $form = $this->createForm(
            RoleType::class,
            $entity,
            array(
                'action' => $this->generateUrl('app_role_update', array('id' => $entity->getId())),
                'method' => 'PUT',
            )
        );

        return $form;
    }

    /**
     * @Route("/{id}", name="app_role_update", methods={"PUT"})
     *
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function updateAction(Request $request, $id): RedirectResponse
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository(Role::class)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Entity not found Role.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            $this->addFlash('success', 'Role guardado correctamente');

            return $this->redirectToRoute('app_role_show', array('id' => $id));
        }

        return $this->redirectToRoute('app_role_edit', array('id' => $id));
    }

    /**
     * @Route("/{id}", name="app_role_delete", methods={"DELETE"})
     *
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, $id): RedirectResponse
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository(Role::class)->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Entity not found Role.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirectToRoute('app_role');
    }

    /**
     * @param mixed $id
     *
     * @return FormInterface
     */
    private function createDeleteForm($id): FormInterface
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('app_role_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add(
                'submit',
                SubmitType::class,
                array('label' => 'label.delete', 'attr' => array('class' => 'btn-danger'))
            )
            ->getForm();
    }
}
