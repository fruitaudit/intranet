<?php

namespace AppBundle\Controller;

use AppBundle\Datatables\EstadisticasProductosDatatable;
use AppBundle\Datatables\PreciosActProductosDatatable;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * EstadisticasProductosController
 *
 * @Route("productos")
 */
class EstadisticasProductosController extends Controller
{
    /**
     * @Route("/", name="estadisticas_productos", methods={"GET"})
     * @throws Exception
     */
    public function indexAction(): Response
    {
        $datatable = $this->get('sg_datatables.factory')->create(EstadisticasProductosDatatable::class);
        $datatable->buildDatatable(['isGranted' => $this->isGranted('ROLE_ADMIN')]);

        return $this->render('AppBundle:estadisticasproductos:index.html.twig', array('datatable' => $datatable));
    }

    /**
     * @Route("/ajax-update/", name="estadisticas_productos_ajax_update", methods={"POST"})
     * @return JsonResponse
     */
    public function ajaxUpdate(): ?JsonResponse
    {
        try {
            $datatable = $this->get('sg_datatables.factory')->create(EstadisticasProductosDatatable::class);
            $datatable->buildDatatable(['isGranted' => $this->isGranted('ROLE_ADMIN')]);
            $responseService = $this->get('sg_datatables.response');
            $responseService->setDatatable($datatable);
            $responseService->getDatatableQueryBuilder();

            return $responseService->getResponse();
        } catch (\Exception $exception) {
            return new JsonResponse(array('error' => 'ajax-update'));
        }
    }

    /**
     * @Route("/{id}", name="estadisticas_precios_producto", methods={"GET"})
     * @param $id
     * @return Response
     * @throws Exception
     */
    public function showAction($id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $entity = $entityManager->getRepository('AppBundle:PreciosProductos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Entity not found PreciosProductos.');
        }

        $datatable = $this->get(PreciosActProductosDatatable::class);
        $datatable->buildDatatable(['isGranted' => $this->isGranted('ROLE_ADMIN'), 'routeId' => $id]);

        return $this->render(
            'AppBundle:estadisticasproductos:show.html.twig',
            array('datatable' => $datatable, 'producto' => $entity)
        );
    }

    /**
     * @Route("/{id}", name="estadisticas_precios_producto_ajax_update", methods={"POST"})
     * @param $id
     * @return Response
     * @throws Exception
     */
    public function showAjaxUpdateAction($id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $entity = $entityManager->getRepository('AppBundle:PreciosProductos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Entity not found PreciosProductos.');
        }

        $datatable = $this->get('sg_datatables.factory')->create(PreciosActProductosDatatable::class);

        try {
            $datatable->buildDatatable(['isGranted' => $this->isGranted('ROLE_ADMIN'), 'routeId' => $id]);

            $responseService = $this->get('sg_datatables.response');
            $responseService->setDatatable($datatable);

            $responseService->getDatatableQueryBuilder()->getQb()
                ->where('preciosactprod.producto = :producto')
                ->setParameter('producto', $entity);

            return $responseService->getResponse();
        } catch (Exception $exception) {
            return new JsonResponse(['error' => $exception->getMessage()]);
        }
    }
}
