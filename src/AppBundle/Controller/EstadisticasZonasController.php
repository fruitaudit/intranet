<?php

namespace AppBundle\Controller;

use AppBundle\Entity\PreciosZonas;
use AppBundle\Entity\User;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * EstadisticasZonasController controller.
 *
 * @Route("zonas")
 */
class EstadisticasZonasController extends Controller
{
    /**
     * @Route("/", name="estadisticas_zonas", methods={"GET"})
     * @throws Exception
     */
    public function indexAction(): Response
    {
        $user = $this->getUser();

        if ($user instanceof User) {
            $zonas = $user->getZonasActivas();
        } else {
            $zonas = $this->get('doctrine.orm.entity_manager')->getRepository(PreciosZonas::class)->findBy(
                ['activo' => 1]
            );
        }

        return $this->render('AppBundle:estadisticaszonas:index.html.twig', array('zonas' => $zonas));
    }
}
