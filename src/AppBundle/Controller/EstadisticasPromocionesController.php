<?php

namespace AppBundle\Controller;

use AppBundle\Datatables\EstadisticasPromocionesDatatable;
use AppBundle\Datatables\EstadisticasPromocionesSupermercadoDatatable;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * EstadisticasPromocionesController
 *
 * @Route("promociones")
 */
class EstadisticasPromocionesController extends Controller
{
    /**
     * @Route("/", name="estadisticas_promociones", methods={"GET"})
     * @throws Exception
     */
    public function indexAction(): Response
    {
        $datatable = $this->get('sg_datatables.factory')->create(EstadisticasPromocionesDatatable::class);
        $datatable->buildDatatable(['isGranted' => $this->isGranted('ROLE_ADMIN')]);

        return $this->render('AppBundle:estadisticaspromociones:index.html.twig', array('datatable' => $datatable));
    }

    /**
     * @Route("/ajax-update/", name="estadisticas_promociones_ajax_update", methods={"POST"})
     * @return JsonResponse
     */
    public function ajaxUpdate(): ?JsonResponse
    {
        try {
            $datatable = $this->get('sg_datatables.factory')->create(EstadisticasPromocionesDatatable::class);
            $datatable->buildDatatable(['isGranted' => $this->isGranted('ROLE_ADMIN')]);
            $responseService = $this->get('sg_datatables.response');
            $responseService->setDatatable($datatable);
            $responseService->getDatatableQueryBuilder();

            return $responseService->getResponse();
        } catch (Exception $exception) {
            return new JsonResponse(array('error' => 'ajax-update'));
        }
    }

    /**
     * @Route("/supermercado/{supermercadoId}/", name="estadisticas_promociones_supermercado", methods={"GET"})
     * @param int $supermercadoId
     * @return Response
     * @throws Exception
     */
    public function supermercadoAction(int $supermercadoId): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $supermercado = $entityManager->getRepository('AppBundle:PreciosSupermercado')->find($supermercadoId);

        if (!$supermercado) {
            throw $this->createNotFoundException('Entity not found PreciosProductos.');
        }

        $datatable = $this->get('sg_datatables.factory')->create(EstadisticasPromocionesSupermercadoDatatable::class);
        $datatable->buildDatatable(['isGranted' => $this->isGranted('ROLE_ADMIN'), 'routeId' => $supermercadoId]);

        return $this->render(
            'AppBundle:estadisticaspromociones:supermercado.html.twig',
            ['datatable' => $datatable, 'supermercado' => $supermercado]
        );
    }

    /**
     * @Route("/supermercado/{supermercadoId}/ajax-update/", name="estadisticas_promociones_supermercado_ajax_update", methods={"POST"})
     * @param int $supermercadoId
     * @return JsonResponse
     */
    public function supermercadoAjaxUpdate(int $supermercadoId): ?JsonResponse
    {
        $entityManager = $this->getDoctrine()->getManager();

        $supermercado = $entityManager->getRepository('AppBundle:PreciosSupermercado')->find($supermercadoId);

        if (!$supermercado) {
            throw $this->createNotFoundException('Entity not found PreciosProductos.');
        }

        try {
            $datatable = $this->get('sg_datatables.factory')->create(
                EstadisticasPromocionesSupermercadoDatatable::class
            );

            $datatable->buildDatatable(['isGranted' => $this->isGranted('ROLE_ADMIN'), 'routeId' => $supermercadoId]);
            $responseService = $this->get('sg_datatables.response');
            $responseService->setDatatable($datatable);

            $responseService->getDatatableQueryBuilder()->getQb()
                ->where('promociones.supermercado = :supermercado')
                ->setParameter('supermercado', $supermercado);

            return $responseService->getResponse();
        } catch (Exception $exception) {
            return new JsonResponse(array('error' => $exception->getMessage()));
        }
    }

    /**
     * @Route("/{id}", name="estadisticas_promociones_show", methods={"GET"})
     * @param $id
     * @return Response
     */
    public function showAction($id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $entity = $entityManager->getRepository('AppBundle:Promociones')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Entity not found Promociones.');
        }

        return $this->render(
            'AppBundle:estadisticaspromociones:show.html.twig',
            array(
                'entity' => $entity,
            )
        );
    }
}
