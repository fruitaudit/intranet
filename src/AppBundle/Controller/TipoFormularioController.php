<?php

namespace AppBundle\Controller;

use AppBundle\Datatables\TipoFormularioDatatable;
use AppBundle\Entity\TipoFormulario;
use AppBundle\Form\TipoFormularioType;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Tipoformulario controller.
 *
 * @Route("admin/tipoformulario")
 */
class TipoFormularioController extends Controller
{
    /**
     * @Route("/", name="admin_tipoformulario", methods={"GET"})
     * @throws Exception
     */
    public function indexAction(): Response
    {
        $datatable = $this->get('sg_datatables.factory')->create(TipoFormularioDatatable::class);
        $datatable->buildDatatable();

        return $this->render('AppBundle:tipoformulario:index.html.twig', array('datatable' => $datatable));
    }

    /**
     * @Route("/ajax-update/", name="admin_tipoformulario_ajax_update", methods={"POST"})
     * @return JsonResponse
     */
    public function ajaxUpdate(): ?JsonResponse
    {
        try {
            $datatable = $this->get('sg_datatables.factory')->create(TipoFormularioDatatable::class);
            $datatable->buildDatatable();
            $responseService = $this->get('sg_datatables.response');
            $responseService->setDatatable($datatable);
            $responseService->getDatatableQueryBuilder();

            return $responseService->getResponse();
        } catch (\Exception $exception) {
            return new JsonResponse(array('error' => 'ajax-update'));
        }
    }

    /**
     * @Route("/", name="admin_tipoformulario_create", methods={"POST"})
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function createAction(Request $request)
    {
        $entity = new TipoFormulario();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($entity);
            $entityManager->flush();

            $this->addFlash('success', 'TipoFormulario guardado correctamente');

            return $this->redirectToRoute('admin_tipoformulario_show', array('id' => $entity->getId()));
        }

        return $this->render(
            'AppBundle:tipoformulario:new.html.twig',
            array(
                'entity' => $entity,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * @param TipoFormulario $entity
     *
     * @return FormInterface
     */
    private function createCreateForm(TipoFormulario $entity): FormInterface
    {
        $form = $this->createForm(
            TipoFormularioType::class,
            $entity,
            array(
                'action' => $this->generateUrl('admin_tipoformulario_create'),
                'method' => 'POST',
            )
        );

        return $form;
    }

    /**
     * @Route("/new", name="admin_tipoformulario_new", methods={"GET"})
     */
    public function newAction(): Response
    {
        $entity = new TipoFormulario();
        $form = $this->createCreateForm($entity);

        return $this->render(
            'AppBundle:tipoformulario:new.html.twig',
            array(
                'entity' => $entity,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * @Route("/{id}", name="admin_tipoformulario_show", methods={"GET"})
     * @param $id
     * @return Response
     */
    public function showAction($id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $entity = $entityManager->getRepository('AppBundle:TipoFormulario')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Entity not found TipoFormulario.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render(
            'AppBundle:tipoformulario:show.html.twig',
            array(
                'entity' => $entity,
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * @Route("/{id}/edit", name="admin_tipoformulario_edit", methods={"GET"})
     * @param $id
     * @return Response
     */
    public function editAction($id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $entity = $entityManager->getRepository('AppBundle:TipoFormulario')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Entity not found TipoFormulario.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render(
            'AppBundle:tipoformulario:edit.html.twig',
            array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * @param TipoFormulario $entity
     *
     * @return FormInterface
     */
    private function createEditForm(TipoFormulario $entity): FormInterface
    {
        $form = $this->createForm(
            TipoFormularioType::class,
            $entity,
            array(
                'action' => $this->generateUrl('admin_tipoformulario_update', array('id' => $entity->getId())),
                'method' => 'PUT',
            )
        );

        return $form;
    }

    /**
     * @Route("/{id}", name="admin_tipoformulario_update", methods={"PUT"})
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function updateAction(Request $request, $id): RedirectResponse
    {
        $entityManager = $this->getDoctrine()->getManager();

        $entity = $entityManager->getRepository('AppBundle:TipoFormulario')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Entity not found TipoFormulario.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $entityManager->flush();
            $this->addFlash('success', 'TipoFormulario guardado correctamente');

            return $this->redirectToRoute('admin_tipoformulario_show', array('id' => $id));
        }

        return $this->redirectToRoute('admin_tipoformulario_edit', array('id' => $id));
    }

    /**
     * @Route("/{id}", name="admin_tipoformulario_delete", methods={"DELETE"})
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, $id): RedirectResponse
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entity = $entityManager->getRepository('AppBundle:TipoFormulario')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Entity not found TipoFormulario.');
            }

            $entityManager->remove($entity);
            $entityManager->flush();

            $this->addFlash('success', 'TipoFormulario eliminado correctamente');
        }

        return $this->redirectToRoute('admin_tipoformulario');
    }

    /**
     * @param mixed $id The entity id
     *
     * @return FormInterface
     */
    private function createDeleteForm($id): FormInterface
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_tipoformulario_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', SubmitType::class, array('label' => 'Delete', 'attr' => array('class' => 'btn-danger')))
            ->getForm();
    }
}
