<?php


namespace AppBundle\Model;


class DirectorioSearch
{
    /** @var string|null */
    private $name;

    /** @var string|null */
    private $ratio;

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return DirectorioSearch
     */
    public function setName(?string $name): DirectorioSearch
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getRatio(): ?string
    {
        return $this->ratio;
    }

    /**
     * @param string|null $ratio
     * @return DirectorioSearch
     */
    public function setRatio(?string $ratio): DirectorioSearch
    {
        $this->ratio = $ratio;

        return $this;
    }
}