const onClickButtonSubmit = function () {
    if ($(this).text() === "Delete") {
        return confirm("¿Are you sure?");
    }

    if ($(this).text() === "Eliminar") {
        return confirm("¿Seguro que quieres eliminar el registro?");
    }
};

const onClickMenuOption = function () {
    let activeMenu = $(this).data("menu");
    localStorage.setItem("activeMenu", activeMenu);
};

const onClickSupplierOption = function () {
    let activeMenu = $(this).data("active");
    localStorage.setItem("activeMenu", activeMenu);
};

const setActiveMenu = function () {
    let activeMenuData = localStorage.getItem("activeMenu");
    if (!activeMenuData) {
        return;
    }

    let activeMenu = $("li[data-menu=" + activeMenuData + "]");
    if (!activeMenu) {
        return;
    }

    activeMenu.addClass('active');

    let parentMenuData = activeMenu.data("parent");
    if (!parentMenuData) {
        return;
    }

    $("li[data-menu=" + parentMenuData + "]").addClass('active');
};

const initializeScrollManager = function () {
    let ScrollManager = {
        current: 0,
        next: function () {
            ScrollManager.current++;

            if (ScrollManager.scrollToCurrent() === false) {
                ScrollManager.reset();
                ScrollManager.scrollToCurrent();
            }
        },
        prev: function () {
            ScrollManager.current--;

            if (ScrollManager.current < 0 || ScrollManager.scrollToCurrent() === false) {
                ScrollManager.current = $('table.dataTable .red').length - 1;
                ScrollManager.scrollToCurrent();
            }
        },
        reset: function () {
            ScrollManager.current = 0;
        },
        scrollToCurrent: function () {
            let currentElements = $('table.dataTable .red');

            if (!(ScrollManager.current in currentElements)) {
                return false;
            }

            let element = $('.current');
            if (element.length !== 0) {
                element.removeClass('current');
            }

            element = $(currentElements[ScrollManager.current]);
            element.addClass('current');

            let scrollTop = element.offset().top - 128;

            let stickyPosition = $('.row.row-sticky').css('position');
            if (stickyPosition != 'fixed') {
                scrollTop -= 190;
            }

            $([document.documentElement, document.body]).animate({scrollTop: scrollTop}, 512);

            return true;
        }
    };

    return ScrollManager;
};

const resetCollectionsHighlight = function (collectionColumn) {
    $('.table tbody tr').each(function (key, value) {
        let element = $(value).children("td:eq(" + collectionColumn + ")");
        if (element.hasClass('red')) {
            element.removeClass('red');
        }
    });
};

const highlightAllCollections = function (e, collectionColumn) {
    e.preventDefault();

    resetCollectionsHighlight(collectionColumn);

    $('.next-collection').hide();
    $('.previous-collection').hide();

    let currentCollection = null;
    let addClass = false;

    $('.table tbody tr').each(function (key, value) {
        let element = $(value).children("td:eq(" + collectionColumn + ")");

        if (currentCollection !== element.html()) {
            currentCollection = element.html();
            addClass = !addClass;
        }

        if (addClass) {
            element.addClass('red');
        }
    });
};

const highlightMultipleCollections = function (e, collectionColumn, ScrollManager) {
    e.preventDefault();

    resetCollectionsHighlight(collectionColumn);

    $('.next-collection').show();
    $('.previous-collection').show();

    let currentCollection = null;
    let elements = null;
    let multipleCollections = false;

    $('.table tbody tr').each(function (key, value) {
        let element = $(value).children("td:eq(" + collectionColumn + ")");

        if (element.html() === '') {
            return;
        }

        if (currentCollection !== element.html()) {
            currentCollection = element.html();
            elements = [element];
        } else {
            elements.push(element);
        }

        if (elements.length > 1) {
            multipleCollections = true;
            for (let i = 0; i < elements.length; i++) {
                elements[i].addClass('red');
            }
        }
    });

    if (multipleCollections === false) {
        alert('No hay colecciones repetidas');
        return;
    }

    ScrollManager.scrollToCurrent();
};

const setActiveSupplier = function () {
    let activeSupplier = $("#supplier-menu a.active").text();
    let text = activeSupplier !== "" ? activeSupplier : 'Selecciona supplier';
    $('#selectedSupplier').html(text);
};

$(document).ready(function () {
    $("button[type='submit']").on("click", onClickButtonSubmit);
    $(".save-active-menu").on("click", onClickMenuOption);
    $(".change-active-menu").on("click", onClickSupplierOption);

    setActiveMenu();
    setActiveSupplier();
});