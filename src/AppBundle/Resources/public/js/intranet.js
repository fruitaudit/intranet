if (localStorage.getItem("menuVisible") < 0) {
    $("body").addClass("sidebar-collapse");
}

const showLoading = function () {
    $('.modal.modal-loading').modal({
        backdrop: 'static',
        keyboard: false
    });
};

const hideLoading = function () {
    $('.modal.modal-loading').modal('hide');
};

$(document).ready(function () {
    $(".navbar .sidebar-toggle").on("click", function () {
        let visible = localStorage.getItem("menuVisible");

        if (visible === undefined || visible == null) {
            visible = 1;
        }

        localStorage.setItem("menuVisible", visible * (-1));
    });

    $(".select2-autocomplete").select2({
        language: "es",
        minimumInputLength: 3,
        allowClear: true,
        placeholder: ""
    });

    $(".ajax-form").submit(function (e) {
        e.preventDefault();
        e.stopPropagation();

        let formUrl = $(this).attr('action');
        let formMethod = $(this).attr('method');
        let formData = new FormData($(this)[0]);

        $.ajax({
            type: formMethod,
            url: formUrl,
            data: formData,
            contentType: false,
            processData: false,
            cache: false,
            beforeSend: function (xhr) {
                showLoading();
            },
            success: function (data, textStatus, jqXHR) {
                location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(textStatus + ': ' + errorThrown);
            },
            complete: function (jqXHR, textStatus) {
                hideLoading();
            }
        });
    });


    $('.boton_volver').on('click', function () {
        window.history.back();
    });


});

window.onscroll = function () {
    if (document.body.scrollTop > 128 || document.documentElement.scrollTop > 128) {
        $('.row.row-sticky').addClass('sticky');
    } else {
        $('.row.row-sticky').removeClass('sticky');
    }
};