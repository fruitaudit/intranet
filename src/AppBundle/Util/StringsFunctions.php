<?php

namespace AppBundle\Util;

class StringsFunctions
{
    /**
     * @param $string
     * @return mixed|string
     */
    public static function removeAccents($string)
    {
        $string = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä', 'é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
            array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A', 'e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $string
        );

        $string = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î', 'ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
            array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I', 'o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $string
        );

        $string = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü', 'ñ', 'Ñ', 'ç', 'Ç'),
            array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U', 'n', 'N', 'c', 'C'),
            $string
        );

        return $string;
    }

    /**
     * @param $string
     * @return string
     */
    public static function removeSpaces($string): string
    {
        return trim(str_replace(array(' ', "\n"), '', $string));
    }

    /**
     * @param $string
     * @return string
     */
    public static function removeEndOfLine($string): string
    {
        return trim(str_replace("\n", '', $string));
    }

    /**
     * @param $string
     * @return string
     */
    public static function removeDashes($string): string
    {
        return str_replace('-', '', $string);
    }

    /**
     * @param $string
     * @return string
     */
    public static function photoName($string): string
    {
        return self::removeAccents(self::removeSpaces($string));
    }

    /**
     * @param boolean $bool
     * @return string
     */
    public static function getBooleanString($bool): string
    {
        return $bool ? 'Yes' : 'No';
    }

    /**
     * @param $IBAN
     * @return string
     */
    public static function getCccFromIban($IBAN): string
    {
        $formatedCcc = '';

        if ($IBAN) {
            $str4 = str_split($IBAN, 4);
            $account4 = array($str4[0], $str4[1], $str4[2]);

            $str2 = str_split($str4[3], 2);
            $account2 = $str2[0];

            $account10 = array($str2[1], $str4[4], $str4[5]);

            $formatedCcc = ''.implode(' ', $account4).' '.$account2.' '.implode($account10);
        }

        return $formatedCcc;
    }

    /**
     * @param string $string
     * @param array $exclude
     * @return array
     */
    public static function getNumbersArray(string $string, array $exclude = []): array
    {
        $response = [];
        $matches = preg_split('/\D+/', $string, null, PREG_SPLIT_NO_EMPTY);

        if (!$matches) {
            return $response;
        }

        foreach ($matches as $key => $match) {
            if (\in_array($match, $exclude, false)) {
                continue;
            }

            $response[] = $match;
        }

        return $response;
    }

    /**
     * @param array $delimiters
     * @param string $string
     * @param bool $deleteDelimiters
     * @return array
     */
    public static function multiExplode(array $delimiters, string $string, $deleteDelimiters = true): array
    {
        if ($deleteDelimiters) {
            foreach ($delimiters as $key => $delimiter) {
                $string = trim(str_replace($delimiter, '|', $string));
            }
        } else {
            foreach ($delimiters as $key => $delimiter) {
                $string = trim(str_replace($delimiter, "|$delimiter", $string));
            }
        }

        return explode('|', trim($string));
    }

    /**
     * @param $text
     * @return string
     */
    public static function capitalizeWords($text): string
    {
        return ucwords(strtolower($text));
    }

    /**
     * @param string $url
     * @return mixed
     */
    public static function encondeStringToUrl(string $url)
    {
        return str_replace(' ', '%20', $url);
    }
}