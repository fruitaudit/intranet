<?php

namespace AppBundle\Service;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

/**
 * Class UserService
 * @package AppBundle\Service
 */
class UserService
{
    /** @var string */
    private $uploadDir;

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var MediaFileService */
    private $mediaFileService;

    /** @var EncoderFactoryInterface */
    private $encoderFactory;

    /**
     * UserService constructor.
     * @param string $uploadDir
     * @param EntityManagerInterface $entityManager
     * @param MediaFileService $mediaFileService
     * @param EncoderFactoryInterface $encoderFactory
     */
    public function __construct(
        string $uploadDir,
        EntityManagerInterface $entityManager,
        MediaFileService $mediaFileService,
        EncoderFactoryInterface $encoderFactory
    ) {
        $this->uploadDir = $uploadDir;
        $this->entityManager = $entityManager;
        $this->mediaFileService = $mediaFileService;
        $this->encoderFactory = $encoderFactory;
    }

    /**
     * @param User $user
     */
    public function encodePassword(User $user): void
    {
        $encoder = $this->encoderFactory->getEncoder($user);
        $encodedPassword = $encoder->encodePassword(
            $user->getPassword(),
            $user->getSalt()
        );

        $user->setPassword($encodedPassword);

        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }

    /**
     * @param User $user
     */
    public function uploadAvatar(User $user): void
    {
        $uploadedFile = $user->getUploadedFile();

        if ($uploadedFile) {
            $oldFile = $user->getAvatar();
            $newFile = $user->getId().'.'.$uploadedFile->getClientOriginalExtension();
            $uploadDir = $this->uploadDir;

            if ($this->mediaFileService->upload($uploadedFile, $uploadDir, $oldFile, $newFile)) {
                $user->setAvatar($newFile);

                $this->entityManager->persist($user);
                $this->entityManager->flush();
            }
        }
    }

    /**
     * @param $id
     */
    public function remove($id): void
    {
        $entity = $this->entityManager->getRepository(User::class)->findOneBy(['id' => $id]);
        if (!$entity) {
            return;
        }

        $this->entityManager->remove($entity);
        $this->entityManager->flush();

        $avatar = $entity->getAvatar();
        if ($avatar !== 'avatar.png' && $avatar !== '') {
            $this->mediaFileService->remove($this->uploadDir, $avatar);
        }
    }

    /**
     * @return User
     */
    public function create(): User
    {
        return new User();
    }

    /**
     * @param User $user
     */
    public function save(User $user): void
    {
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }

    /**
     * @param int $id
     * @return User|object
     */
    public function find(int $id): User
    {
        return $this->entityManager->getRepository(User::class)->find($id);
    }

    /**
     * @param array $criteria
     * @return User[]|array
     */
    public function findBy(array $criteria = []): array
    {
        return $this->entityManager->getRepository(User::class)->findBy($criteria);
    }

    /**
     * @param User $user
     * @return User
     */
    public function setLastAccess(User $user): User
    {
        $user->setLastAccess(new \DateTime());

        return $user;
    }

    /**
     * @param User $user
     * @param string $sessionId
     * @return User
     */
    public function setSession(User $user, string $sessionId): User
    {
        $user->setSession($sessionId);

        return $user;
    }
}