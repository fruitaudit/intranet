<?php

namespace AppBundle\Service;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class MediaFileService
{
    /**
     * @param $uploadedFile
     * @param $route
     * @param $oldFile
     * @param $newFile
     * @return bool|File
     */
    public function upload(UploadedFile $uploadedFile = null, $route, $oldFile, $newFile)
    {
        if (null === $uploadedFile) {
            return false;
        }

        if (is_file($route.$oldFile)) {
            unlink($route.$oldFile);
        }

        return $uploadedFile->move($route, $newFile);
    }

    /**
     * @param $route
     * @param $file
     * @return bool
     */
    public function remove($route, $file): bool
    {
        if (is_file($route.$file)) {
            unlink($route.$file);
        }

        return true;
    }
}