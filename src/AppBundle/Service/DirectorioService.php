<?php

namespace AppBundle\Service;

use AppBundle\Entity\Directorio;
use Doctrine\ORM\EntityManagerInterface;


class DirectorioService
{
    /** @var EntityManagerInterface */
    private $entityManager;


    /**
     * DirectorioService constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return Directorio
     */
    public function create(): Directorio
    {
        return new Directorio();
    }

    /**
     * @param array $criteria
     * @param array $order
     * @return Directorio[]|array
     */
    public function findBy(array $criteria, array $order): array
    {
        return $this->entityManager->getRepository(Directorio::class)->findBy($criteria, $order);
    }
}