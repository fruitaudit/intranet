<?php

namespace AppBundle\Service;

use Twig\Environment as TwigEnvironment;

/**
 * Class MailerService
 * @package AppBundle\Service
 */
class MailerService
{
    /** @var \Swift_Mailer */
    private $memoryMailer;

    /** @var \Swift_Mailer */
    private $queueMailer;

    /** @var TwigEnvironment */
    private $twig;

    /** @var string */
    private $from;

    /**
     * @param \Swift_Mailer $memoryMailer
     * @param \Swift_Mailer $queueMailer
     * @param TwigEnvironment $twig
     * @param string $from
     */
    public function __construct(
        \Swift_Mailer $memoryMailer,
        \Swift_Mailer $queueMailer,
        TwigEnvironment $twig,
        string $from
    ) {
        $this->memoryMailer = $memoryMailer;
        $this->queueMailer = $queueMailer;
        $this->twig = $twig;
        $this->from = $from;
    }

    /**
     * @param string|array $to
     * @param string $subject
     * @param string $body
     * @param array $params
     * @return int
     */
    public function sendEmail($to, $subject, $body, array $params = array()): int
    {
        $message = $this->getMessage($to, $subject, $params);

        $message->setBody($body);

        return $this->memoryMailer->send($message);
    }

    /**
     * @param string $to
     * @param string $subject
     * @param string $body
     * @param array $params
     * @return int
     */
    public function sendQueueEmail($to, $subject, $body, array $params = array()): int
    {
        $message = $this->getMessage($to, $subject, $params);

        $message->setBody($body, 'text/html');

        return $this->queueMailer->send($message);
    }

    /**
     * @param string $to
     * @param string $subject
     * @param string $template
     * @param array $params
     * @return int
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function sendTemplateEmail($to, $subject, $template, array $params = array()): int
    {
        $message = $this->getMessage($to, $subject, $params);
        $message->setBody($this->twig->render($template, $params), 'text/html');

        return $this->memoryMailer->send($message);
    }

    /**
     * @param $to
     * @param $subject
     * @param $template
     * @param array $params
     * @return int
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function sendQueueTemplateEmail($to, $subject, $template, array $params = array()): int
    {
        $message = $this->getMessage($to, $subject, $params);
        $message->setBody($this->twig->render($template, $params), 'text/html');

        return $this->queueMailer->send($message);
    }

    /**
     * @param string|array $addresses
     * @param string $subject
     * @param array $params
     * @return \Swift_Message
     */
    private function getMessage($addresses, string $subject, array $params = array()): \Swift_Message
    {
        $message = new \Swift_Message($subject, null, 'text/html', 'UTF-8');

        $message->setTo($addresses);
        $message->setSender($this->from);
        $message->setReturnPath($this->from);

        if (empty($params)) {
            return $message;
        }

        if (isset($params['from']) && !empty(trim($params['from']))) {
            $message->setFrom($params['from']);
        } else {
            $message->setFrom($this->from);
        }

        if (isset($params['cc'])) {
            if (\is_array($params['cc']) && \count($params['cc']) > 1) {
                foreach ($params['cc'] as $cc) {
                    $message->addCc($cc);
                }
            } else {
                $message->setCc($params['cc']);
            }
        }

        if (isset($params['bcc'])) {
            if (\is_array($params['bcc']) && \count($params['bcc']) > 1) {
                foreach ($params['bcc'] as $bcc) {
                    $message->addBcc($bcc);
                }
            } else {
                $message->setBcc($params['bcc']);
            }
        }

        if (isset($params['replyTo']) && $params['replyTo'] !== '') {
            $message->setReplyTo($params['replyTo']);
        }

        if (!empty($params['attachment'])) {
            $attachments = $params['attachment'];

            if (!\is_array($attachments)) {
                $attachments = array($attachments);
            }

            foreach ($attachments as $path) {
                $message->attach(\Swift_Attachment::fromPath($path));
            }
        }

        return $message;
    }
}