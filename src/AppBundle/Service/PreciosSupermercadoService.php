<?php

namespace AppBundle\Service;

use AppBundle\Entity\PreciosSupermercado;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class PreciosSupermercadoService
 * @package AppBundle\Service
 */
class PreciosSupermercadoService
{
    /** @var string */
    private $uploadDir;

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var MediaFileService */
    private $mediaFileService;

    /**
     * UserService constructor.
     * @param string $uploadDir
     * @param EntityManagerInterface $entityManager
     * @param MediaFileService $mediaFileService
     */
    public function __construct(
        string $uploadDir,
        EntityManagerInterface $entityManager,
        MediaFileService $mediaFileService
    ) {
        $this->uploadDir = $uploadDir;
        $this->entityManager = $entityManager;
        $this->mediaFileService = $mediaFileService;
    }

    /**
     * @param PreciosSupermercado $preciosSupermercado
     */
    public function uploadImage(PreciosSupermercado $preciosSupermercado): void
    {
        $uploadedFile = $preciosSupermercado->getUploadedFile();

        if ($uploadedFile) {
            $oldFile = $preciosSupermercado->getImg();
            $newFile = $preciosSupermercado->getId().'.'.$uploadedFile->getClientOriginalExtension();
            $uploadDir = $this->uploadDir;

            if ($this->mediaFileService->upload($uploadedFile, $uploadDir, $oldFile, $newFile)) {
                $preciosSupermercado->setImg($newFile);

                $this->entityManager->persist($preciosSupermercado);
                $this->entityManager->flush();
            }
        }
    }

    /**
     * @param PreciosSupermercado $preciosSupermercado
     */
    public function save(PreciosSupermercado $preciosSupermercado): void
    {
        $this->entityManager->persist($preciosSupermercado);
        $this->entityManager->flush();
    }
}