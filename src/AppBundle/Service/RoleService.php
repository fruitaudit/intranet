<?php

namespace AppBundle\Service;

use AppBundle\Entity\Role;
use Doctrine\ORM\EntityManagerInterface;

class RoleService
{
    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return Role
     */
    public function create(): Role
    {
        return new Role();
    }

    /**
     * @param array $roleData
     * @return Role
     */
    public function createFromArray(array $roleData): Role
    {
        $role = new Role();
        $role->setName($roleData['name']);
        $role->setCode($roleData['code']);

        return $role;
    }

    /**
     * @param array $criteria
     * @return Role[]|array
     */
    public function findBy(array $criteria): array
    {
        return $this->entityManager->getRepository(Role::class)->findBy($criteria);
    }

}