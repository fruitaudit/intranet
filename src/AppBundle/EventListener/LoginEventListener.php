<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\User;
use AppBundle\Service\UserService;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

/**
 * Class LoginListener
 * @package AppBundle\EventListener
 */
class LoginEventListener
{
    /** @var UserService */
    private $userService;

    /** @var SessionInterface */
    private $session;

    /**
     * LoginListener constructor.
     * @param UserService $userService
     * @param SessionInterface $session
     */
    public function __construct(UserService $userService, SessionInterface $session)
    {
        $this->userService = $userService;
        $this->session = $session;
    }

    /**
     * @param InteractiveLoginEvent $event
     */
    public function onInteractiveLogin(InteractiveLoginEvent $event): void
    {
        $user = $event->getAuthenticationToken()->getUser();
        if (!$user) {
            return;
        }

        if (!$user instanceof User) {
            return;
        }

        $this->userService->setLastAccess($user);
        $this->userService->setSession($user, $this->session->getId());

        $this->userService->save($user);
    }
}