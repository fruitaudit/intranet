<?php

namespace AppBundle\DataFixtures;

use AppBundle\Service\RoleService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Roles
 *
 * @package AppBundle\MOP\DataFixtures
 */
class Roles extends Fixture implements ContainerAwareInterface
{
    /** @var ContainerInterface */
    private $container;

    /** @var array */
    private $rolesData = [
        ['name' => 'Administrador', 'code' => 'ROLE_ADMIN'],
        ['name' => 'Perito', 'code' => 'ROLE_PERITO'],
        ['name' => 'Usuario', 'code' => 'ROLE_USER'],
    ];

    /**
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @param ObjectManager $objectManager
     */
    public function load(ObjectManager $objectManager)
    {
        $roleService = $this->container->get(RoleService::class);

        foreach ($this->rolesData as $rolesData) {
            $role = $roleService->findBy(array('code' => $rolesData['code']));
            if ($role) {
                continue;
            }

            $role = $roleService->createFromArray($rolesData);
            $objectManager->persist($role);
        }

        $objectManager->flush();
    }
}
