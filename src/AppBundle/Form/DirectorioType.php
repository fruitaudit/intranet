<?php

namespace AppBundle\Form;

use AppBundle\Entity\Categoria;
use AppBundle\Entity\Directorio;
use AppBundle\Entity\PreciosZonas;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DirectorioType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $entity = $builder->getData();

        $builder
            ->add('nombre')
            ->add('direccion')
            ->add('telefono')
            ->add('provincia')
            ->add(
                'zona',
                EntityType::class,
                [
                    'required' => false,
                    'class' => PreciosZonas::class,
                    'query_builder' => static function (EntityRepository $repository) {
                        return $repository
                            ->createQueryBuilder('c')
                            ->orderBy('c.nombre', 'ASC');
                    },
                    'choice_label' => 'nombre',
                ]
            )
            ->add('poblacion')
            ->add('cp')
            ->add('email')
            ->add('web')
            ->add('contacto')
            ->add('info')
            ->add('gmaps')
            ->add(
                'sucursal',
                EntityType::class,
                [
                    'required' => false,
                    'class' => Directorio::class,
                    'query_builder' => static function (EntityRepository $repository) use ($entity) {
                        return $repository
                            ->createQueryBuilder('d')
                            ->where('d.sucursal IS NULL')
                            ->andWhere('d.id <> :id')
                            ->setParameter('id', $entity)
                            ->orderBy('d.nombre', 'ASC');
                    },
                    'choice_label' => 'nombre',
                ]
            )
            ->add('cif')
            ->add('latitud')
            ->add('longitud')
            ->add(
                'categoria',
                EntityType::class,
                [
                    'required' => false,
                    'class' => Categoria::class,
                    'query_builder' => static function (EntityRepository $repository) {
                        return $repository
                            ->createQueryBuilder('c')
                            ->orderBy('c.nombre', 'ASC');
                    },
                    'choice_label' => 'nombre',
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => Directorio::class,
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_directorio';
    }
}
