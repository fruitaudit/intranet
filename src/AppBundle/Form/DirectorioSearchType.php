<?php

namespace AppBundle\Form;

use AppBundle\Model\DirectorioSearch;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DirectorioSearchType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name',
                TextType::class,
                [
                    'label' => 'Nombre',
                    'required' => true,
                ]
            )
            ->add(
                'ratio',
                ChoiceType::class,
                [
                    'label' => 'Radio',
                    'required' => true,
                    'choices' => [
                        '0 a 5 KM' => '16',
                        '10 a 15 KM' => '15',
                        '15 a 20 KM' => '14',
                        '20 a 25 KM' => '13',
                        '25 a 50 KM' => '12',
                        '50 a 75 KM' => '11',
                        '75 a 100 km' => '10',
                        '100 a 150 km' => '9',
                        '150 a 200 km' => '8',
                        '200 a 500 km' => '7',
                    ],
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => DirectorioSearch::class]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'directorio_search';
    }
}
