<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FormulariosType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fecha')
            ->add('estado')
            ->add('tipo')
            ->add('cliente')
            ->add('perito')
            ->add('lugarMercancia')
            ->add('fechaHorario')
            ->add('exportadorEnvasador')
            ->add('destinatario')
            ->add('descripcionMercaciaEtiquetado')
            ->add('embalaje')
            ->add('empaquetadoEnvio')
            ->add('condicionesEntrega')
            ->add('transporte')
            ->add('lugarFechaDescarga')
            ->add('especificacionesTecnicas')
            ->add('motivos')
            ->add('contactoOrigenDestino')
            ->add('albaranSalida')
            ->add('otraInformacion');
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'AppBundle\Entity\Formularios',
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_formularios';
    }


}
