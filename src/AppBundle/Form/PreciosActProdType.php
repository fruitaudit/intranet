<?php

namespace AppBundle\Form;

use AppBundle\Entity\PreciosActProd;
use AppBundle\Entity\PreciosProductos;
use AppBundle\Entity\PreciosSupermercado;
use AppBundle\Entity\PreciosZonas;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PreciosActProdType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('envase')
            ->add('origen')
            ->add('precio')
            ->add('estado')
            ->add('fecha')
            ->add('info')
            ->add(
                'producto',
                EntityType::class,
                [
                    'required' => true,
                    'class' => PreciosProductos::class,
                    'query_builder' => static function (EntityRepository $repository) {
                        return $repository
                            ->createQueryBuilder('p')
                            ->orderBy('p.nombre', 'ASC');
                    },
                    'choice_label' => 'nombre',
                ]
            )
            ->add(
                'zona',
                EntityType::class,
                [
                    'required' => true,
                    'class' => PreciosZonas::class,
                    'query_builder' => static function (EntityRepository $repository) {
                        return $repository
                            ->createQueryBuilder('z')
                            ->orderBy('z.nombre', 'ASC');
                    },
                    'choice_label' => 'nombre',
                ]
            )
            ->add(
                'supermercado',
                EntityType::class,
                [
                    'required' => true,
                    'class' => PreciosSupermercado::class,
                    'query_builder' => static function (EntityRepository $repository) {
                        return $repository
                            ->createQueryBuilder('s')
                            ->orderBy('s.nombre', 'ASC');
                    },
                    'choice_label' => 'nombre',
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => PreciosActProd::class,
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_preciosactprod';
    }


}
