<?php

namespace AppBundle\Form;

use AppBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserEditType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name',
                null,
                array(
                    'label' => 'user.name',
                )
            )
            ->add(
                'user',
                null,
                array(
                    'label' => 'Usuario',
                )
            )
            ->add(
                'email',
                null,
                array(
                    'label' => 'user.email',
                )
            )
            ->add(
                'uploadedFile',
                null,
                array(
                    'label' => 'user.avatar',
                )
            )
            ->add(
                'role',
                null,
                array(
                    'label' => 'user.role',
                )
            )
            ->add(
                'active',
                null,
                array(
                    'label' => 'user.active',
                )
            )
            ->add(
                'fechaExp',
                null,
                array(
                    'label' => 'Fecha Exp',
                )
            )
            ->add(
                'provincia',
                null,
                array(
                    'label' => 'Provincia',
                )
            )
            ->add(
                'poblacion',
                null,
                array(
                    'label' => 'Población',
                )
            )
            ->add(
                'cp',
                null,
                array(
                    'label' => 'CP',
                )
            )
            ->add(
                'calle',
                null,
                array(
                    'label' => 'Calle',
                )
            )
            ->add(
                'direccion',
                null,
                array(
                    'label' => 'Dirección',
                )
            )
            ->add(
                'tlf',
                null,
                array(
                    'label' => 'Teléfono',
                )
            )
            ->add(
                'fax',
                null,
                array(
                    'label' => 'Fax',
                )
            )
            ->add(
                'contacto',
                null,
                array(
                    'label' => 'Contacto',
                )
            )
            ->add(
                'cargo',
                null,
                array(
                    'label' => 'Cargo',
                )
            )
            ->add(
                'asociacion',
                null,
                array(
                    'label' => 'Asociación',
                )
            );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => User::class,
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_user_edit';
    }


}
