<?php

namespace AppBundle\Form;

use AppBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name',
                null,
                array(
                    'label' => 'user.name',
                )
            )
            ->add(
                'user',
                null,
                array(
                    'label' => 'Usuario',
                )
            )
            ->add(
                'email',
                null,
                array(
                    'label' => 'user.email',
                )
            )
            ->add(
                'password',
                RepeatedType::class,
                array(
                    'type' => PasswordType::class,
                    'invalid_message' => 'Passwords are not equal',
                    'options' => array(
                        'always_empty' => true,
                        'required' => 'required',
                    ),
                    'first_options' => array(
                        'label' => 'user.password',
                        'attr' => array(
                            'class' => 'text-input-border registro-user-pass',
                        ),
                    ),
                    'second_options' => array(
                        'label' => 'user.password_repeat',
                        'attr' => array(
                            'class' => 'text-input-border registro-user-pass',
                        ),
                    ),
                )
            )
            ->add(
                'uploadedFile',
                null,
                array(
                    'label' => 'user.avatar',
                )
            )
            ->add(
                'role',
                null,
                array(
                    'label' => 'user.role',
                )
            )
            ->add(
                'active',
                null,
                array(
                    'label' => 'user.active',
                )
            );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => User::class,
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_user';
    }


}
