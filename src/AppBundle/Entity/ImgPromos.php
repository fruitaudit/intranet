<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ImgPromos
 *
 * @ORM\Table(name="img_promos")
 * @ORM\Entity
 */
class ImgPromos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idpromo", type="integer", nullable=true)
     */
    private $idpromo = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="imagen", type="string", length=250, nullable=true)
     */
    private $imagen = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


}

