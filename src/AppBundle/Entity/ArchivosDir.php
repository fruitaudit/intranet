<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ArchivosDir
 *
 * @ORM\Table(name="archivos_dir")
 * @ORM\Entity
 */
class ArchivosDir
{
    /**
     * @var string
     *
     * @ORM\Column(name="archivo", type="string", length=100, nullable=false)
     */
    private $archivo;

    /**
     * @var integer
     *
     * @ORM\Column(name="directorio", type="integer", nullable=false)
     */
    private $directorio;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


}

