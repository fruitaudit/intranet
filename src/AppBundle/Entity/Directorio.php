<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Directorio
 *
 * @ORM\Table(name="directorio")
 * @ORM\Entity
 */
class Directorio
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre", type="string", length=250, nullable=true)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="direccion", type="string", length=250, nullable=true)
     */
    private $direccion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tlf", type="string", length=250, nullable=true)
     */
    private $telefono;

    /**
     * @var string|null
     *
     * @ORM\Column(name="provincia", type="string", length=250, nullable=true)
     */
    private $provincia;

    /**
     * @var PreciosZonas|null
     *
     * @ORM\ManyToOne(targetEntity="PreciosZonas", inversedBy="directorios")
     */
    private $zona;

    /**
     * @var string|null
     *
     * @ORM\Column(name="poblacion", type="string", length=250, nullable=true)
     */
    private $poblacion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cp", type="string", length=250, nullable=true)
     */
    private $cp;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=250, nullable=true)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="web", type="string", length=250, nullable=true)
     */
    private $web;

    /**
     * @var string|null
     *
     * @ORM\Column(name="contacto", type="string", length=250, nullable=true)
     */
    private $contacto;

    /**
     * @var string|null
     *
     * @ORM\Column(name="info", type="text", length=65535, nullable=true)
     */
    private $info;

    /**
     * @var string|null
     *
     * @ORM\Column(name="gmaps", type="string", length=250, nullable=true)
     */
    private $gmaps;

    /**
     * @var Directorio|null
     *
     * @ORM\ManyToOne(targetEntity="Directorio", inversedBy="sucursales")
     */
    private $sucursal;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cif", type="string", length=250, nullable=true)
     */
    private $cif;

    /**
     * @var float|null
     *
     * @ORM\Column(name="latitud", type="float", precision=10, scale=0, nullable=true)
     */
    private $latitud;

    /**
     * @var float|null
     *
     * @ORM\Column(name="longitud", type="float", precision=10, scale=0, nullable=true)
     */
    private $longitud;

    /**
     * @var Categoria|null
     *
     * @ORM\ManyToOne(targetEntity="Categoria", inversedBy="directorios")
     */
    private $categoria;

    /**
     * @var Directorio[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Directorio", mappedBy="sucursal")
     */
    private $sucursales;


    public function __construct()
    {
        $this->sucursales = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string|null $nombre
     * @return Directorio
     */
    public function setNombre(?string $nombre): Directorio
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDireccion(): ?string
    {
        return $this->direccion;
    }

    /**
     * @param string|null $direccion
     * @return Directorio
     */
    public function setDireccion(?string $direccion): Directorio
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    /**
     * @param string|null $telefono
     * @return Directorio
     */
    public function setTelefono(?string $telefono): Directorio
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getProvincia(): ?string
    {
        return $this->provincia;
    }

    /**
     * @param string|null $provincia
     * @return Directorio
     */
    public function setProvincia(?string $provincia): Directorio
    {
        $this->provincia = $provincia;

        return $this;
    }

    /**
     * @return PreciosZonas|null
     */
    public function getZona(): ?PreciosZonas
    {
        return $this->zona;
    }

    /**
     * @param PreciosZonas|null $zona
     * @return Directorio
     */
    public function setZona(?PreciosZonas $zona): Directorio
    {
        $this->zona = $zona;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPoblacion(): ?string
    {
        return $this->poblacion;
    }

    /**
     * @param string|null $poblacion
     * @return Directorio
     */
    public function setPoblacion(?string $poblacion): Directorio
    {
        $this->poblacion = $poblacion;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCp(): ?string
    {
        return $this->cp;
    }

    /**
     * @param string|null $cp
     * @return Directorio
     */
    public function setCp(?string $cp): Directorio
    {
        $this->cp = $cp;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     * @return Directorio
     */
    public function setEmail(?string $email): Directorio
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getWeb(): ?string
    {
        return $this->web;
    }

    /**
     * @param string|null $web
     * @return Directorio
     */
    public function setWeb(?string $web): Directorio
    {
        $this->web = $web;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getContacto(): ?string
    {
        return $this->contacto;
    }

    /**
     * @param string|null $contacto
     * @return Directorio
     */
    public function setContacto(?string $contacto): Directorio
    {
        $this->contacto = $contacto;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getInfo(): ?string
    {
        return $this->info;
    }

    /**
     * @param string|null $info
     * @return Directorio
     */
    public function setInfo(?string $info): Directorio
    {
        $this->info = $info;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getGmaps(): ?string
    {
        return $this->gmaps;
    }

    /**
     * @param string|null $gmaps
     * @return Directorio
     */
    public function setGmaps(?string $gmaps): Directorio
    {
        $this->gmaps = $gmaps;

        return $this;
    }

    /**
     * @return Directorio|null
     */
    public function getSucursal(): ?Directorio
    {
        return $this->sucursal;
    }

    /**
     * @param Directorio|null $sucursal
     * @return Directorio
     */
    public function setSucursal(?Directorio $sucursal): Directorio
    {
        $this->sucursal = $sucursal;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCif(): ?string
    {
        return $this->cif;
    }

    /**
     * @param string|null $cif
     * @return Directorio
     */
    public function setCif(?string $cif): Directorio
    {
        $this->cif = $cif;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getLatitud(): ?float
    {
        return $this->latitud;
    }

    /**
     * @param float|null $latitud
     * @return Directorio
     */
    public function setLatitud(?float $latitud): Directorio
    {
        $this->latitud = $latitud;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getLongitud(): ?float
    {
        return $this->longitud;
    }

    /**
     * @param float|null $longitud
     * @return Directorio
     */
    public function setLongitud(?float $longitud): Directorio
    {
        $this->longitud = $longitud;

        return $this;
    }

    /**
     * @return Categoria|null
     */
    public function getCategoria(): ?Categoria
    {
        return $this->categoria;
    }

    /**
     * @param Categoria|null $categoria
     * @return Directorio
     */
    public function setCategoria(?Categoria $categoria): Directorio
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * @return Directorio[]|ArrayCollection
     */
    public function getSucursales()
    {
        return $this->sucursales;
    }

    /**
     * @param Directorio[]|ArrayCollection $sucursales
     * @return Directorio
     */
    public function setSucursales($sucursales)
    {
        $this->sucursales = $sucursales;

        return $this;
    }

    public function __toString()
    {
        return $this->getNombre().'';
    }
}

