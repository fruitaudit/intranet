<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FacturasCampos
 *
 * @ORM\Table(name="facturas_campos")
 * @ORM\Entity
 */
class FacturasCampos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="fac", type="integer", nullable=false)
     */
    private $fac = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="concepto", type="string", length=250, nullable=false)
     */
    private $concepto = '';

    /**
     * @var float
     *
     * @ORM\Column(name="cant", type="float", precision=10, scale=0, nullable=false)
     */
    private $cant = '0';

    /**
     * @var float
     *
     * @ORM\Column(name="precio", type="float", precision=10, scale=0, nullable=false)
     */
    private $precio = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="iva", type="integer", nullable=true)
     */
    private $iva = '21';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


}

