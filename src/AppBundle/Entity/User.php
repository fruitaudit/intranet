<?php

namespace AppBundle\Entity;

use AppBundle\Entity\MOP\Group;
use AppBundle\Util\StringsFunctions;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Serializable;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User
 *
 * @ORM\Table(name="usuarios")
 * @ORM\Entity()
 */
class User implements AdvancedUserInterface, EquatableInterface, Serializable
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\Email(
     *        message="Invalid email '{{ value }}'",
     *        checkMX = true
     * )
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\Length(min=6, minMessage="Password too short")
     * @Assert\NotBlank(message="Password is required")
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $salt;

    /**
     * @var bool
     *
     * @ORM\Column(name="activo", type="boolean", nullable=true)
     */
    private $active;

    /**
     * @var string
     *
     * @ORM\Column(name="logo", type="string", nullable=true)
     */
    private $avatar;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $session;

    /**
     * @Assert\Image(maxSize = "3500k")
     */
    private $uploadedFile;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\Type(type="\DateTime")
     */
    private $lastAccess;

    /**
     * @var Role
     *
     * @ORM\ManyToOne(targetEntity="Role")
     * @Assert\NotNull()
     */
    private $role;

    /**
     * @var string|null
     *
     * @ORM\Column(name="direccion", type="text", length=65535, nullable=true)
     */
    private $direccion;

    /**
     * @var string
     *
     * @ORM\Column(name="calle", type="string", length=200, nullable=true)
     */
    private $calle;

    /**
     * @var string
     *
     * @ORM\Column(name="poblacion", type="string", length=100, nullable=true)
     */
    private $poblacion;

    /**
     * @var string
     *
     * @ORM\Column(name="provincia", type="string", length=100, nullable=true)
     */
    private $provincia;

    /**
     * @var string
     *
     * @ORM\Column(name="cp", type="string", length=20, nullable=true)
     */
    private $cp;

    /**
     * @var string
     *
     * @ORM\Column(name="tlf", type="string", length=20, nullable=false)
     */
    private $tlf;

    /**
     * @var string
     *
     * @ORM\Column(name="fax", type="string", length=20, nullable=false)
     */
    private $fax;

    /**
     * @var string
     *
     * @ORM\Column(name="cif", type="string", length=30, nullable=false)
     */
    private $cif;

    /**
     * @var string
     *
     * @ORM\Column(name="contacto", type="string", length=150, nullable=false)
     */
    private $contacto;

    /**
     * @var string
     *
     * @ORM\Column(name="cargo", type="string", length=150, nullable=false)
     */
    private $cargo;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="fecha_exp", type="datetime", nullable=true)
     */
    private $fechaExp;

    /**
     * @var string
     *
     * @ORM\Column(name="user", type="string", length=50, nullable=false)
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="psw", type="string", length=50, nullable=false)
     */
    private $psw;

    /**
     * @var float
     *
     * @ORM\Column(name="tarifa", type="float", precision=10, scale=0, nullable=false)
     */
    private $tarifa;

    /**
     * @var integer
     *
     * @ORM\Column(name="tipo", type="integer", nullable=false)
     */
    private $tipo;

    /**
     * @var string
     *
     * @ORM\Column(name="observaciones", type="text", length=65535, nullable=true)
     */
    private $observaciones;

    /**
     * @var PreciosProductos[]
     *
     * @ORM\ManyToMany(targetEntity="PreciosProductos", inversedBy="usuarios")
     * @ORM\JoinTable(name="usuarios_productos")
     */
    protected $productos;

    /**
     * @var PreciosZonas[]
     *
     * @ORM\ManyToMany(targetEntity="PreciosZonas", inversedBy="usuarios")
     * @ORM\JoinTable(name="usuarios_zonas")
     */
    protected $zonas;

    /**
     * @var Asociaciones
     *
     * @ORM\ManyToOne(targetEntity="Asociaciones", inversedBy="usuarios")
     */
    private $asociacion;

    /**
     * @var MargenesProds[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="MargenesProds", mappedBy="user")
     */
    private $margenes;

    /**
     * @var Ips[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Ips", mappedBy="user")
     */
    private $ips;

    /**
     * @var Access[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Access", mappedBy="user")
     */
    private $accesos;

    /**
     * @var Formularios[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Formularios", mappedBy="cliente")
     */
    private $servicios;

    /**
     * @var Formularios[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Formularios", mappedBy="perito")
     */
    private $formularios;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->salt = md5(time().uniqid('', true));
        $this->active = false;
        $this->margenes = new ArrayCollection();
        $this->ips = new ArrayCollection();
        $this->accesos = new ArrayCollection();
        $this->servicios = new ArrayCollection();
        $this->formularios = new ArrayCollection();
        $this->provincia = new ArrayCollection();
        $this->zonas = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return User
     */
    public function setName(string $name): User
    {
        $this->name = ucwords(mb_strtolower($name));

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName(): ?string
    {
        return ucwords(mb_strtolower($this->name));
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail(string $email): User
    {
        $this->email = mb_strtolower($email);

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail(): ?string
    {
        return mb_strtolower($this->email);
    }

    /**
     * @return string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPassword(string $password): User
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return string
     */
    public function getSalt(): ?string
    {
        return $this->salt;
    }

    /**
     * @param string $salt
     * @return User
     */
    public function setSalt(string $salt): User
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): ?bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return User
     */
    public function setActive(bool $active): User
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return string
     */
    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    /**
     * @param string $avatar
     * @return User
     */
    public function setAvatar(string $avatar): User
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getLastAccess(): ?DateTime
    {
        return $this->lastAccess;
    }

    /**
     * @param DateTime $lastAccess
     * @return User
     */
    public function setLastAccess(DateTime $lastAccess): User
    {
        $this->lastAccess = $lastAccess;

        return $this;
    }

    /**
     * @return Role|null
     */
    public function getRole(): ?Role
    {
        return $this->role;
    }

    /**
     * @param Role $role
     * @return User
     */
    public function setRole(Role $role): User
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get activeString
     *
     * @return string
     */
    public function isActiveString(): string
    {
        return StringsFunctions::getBooleanString($this->active);
    }

    /**
     * @return string
     */
    public function getRouteAvatar(): string
    {
        $avatar = ($this->avatar !== null && $this->avatar !== '') ? $this->avatar : 'avatar.png';

        return '/uploads/user/'.$avatar;
    }

    /**
     * @param UploadedFile $uploadedFile
     * @return User
     */
    public function setUploadedFile(UploadedFile $uploadedFile = null): User
    {
        $this->uploadedFile = $uploadedFile;

        return $this;
    }

    /**
     * @return UploadedFile
     */
    public function getUploadedFile(): ?UploadedFile
    {
        return $this->uploadedFile;
    }

    /**
     * @return array
     */
    public function getRoles(): array
    {
        $role = $this->getRole();

        return $role ? array($role->getCode()) : array();
    }

    /**
     * @return User
     */
    public function eraseCredentials(): User
    {
        return $this;
    }

    /**
     * @return string
     */
    public function getUsername(): ?string
    {
        return $this->getEmail();
    }

    /**
     * @return bool
     */
    public function isEnabled(): ?bool
    {
        return $this->isActive();
    }

    /**
     * @return bool
     */
    public function isAccountNonExpired(): ?bool
    {
        return $this->isActive();
    }

    /**
     * @return bool
     */
    public function isAccountNonLocked(): ?bool
    {
        return $this->isActive();
    }

    /**
     * @return bool
     */
    public function isCredentialsNonExpired(): ?bool
    {
        return $this->isActive();
    }

    /**
     * @return string
     */
    public function serialize(): ?string
    {
        return serialize(array($this->getId(), $this->getUsername()));
    }

    /**
     * @param $data
     */
    public function unserialize($data)
    {
        [$this->id, $this->email] = unserialize($data);
    }

    /**
     * @param UserInterface $user
     * @return bool
     */
    public function isEqualTo(UserInterface $user): bool
    {
        return $this->email === $user->getUsername();
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getName().'';
    }

    /**
     * @return string|null
     */
    public function getSession(): ?string
    {
        return $this->session;
    }

    /**
     * @param string $session
     * @return User
     */
    public function setSession(string $session): User
    {
        $this->session = $session;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDireccion(): ?string
    {
        return $this->direccion;
    }

    /**
     * @param string $direccion
     * @return User
     */
    public function setDireccion(string $direccion = null): User
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * @return string
     */
    public function getCalle(): string
    {
        return $this->calle;
    }

    /**
     * @param string $calle
     * @return User
     */
    public function setCalle(string $calle): User
    {
        $this->calle = $calle;

        return $this;
    }

    /**
     * @return string
     */
    public function getPoblacion(): string
    {
        return $this->poblacion;
    }

    /**
     * @param string $poblacion
     * @return User
     */
    public function setPoblacion(string $poblacion): User
    {
        $this->poblacion = $poblacion;

        return $this;
    }

    /**
     * @return string
     */
    public function getProvincia(): string
    {
        return $this->provincia;
    }

    /**
     * @param string $provincia
     * @return User
     */
    public function setProvincia(string $provincia): User
    {
        $this->provincia = $provincia;

        return $this;
    }

    /**
     * @return string
     */
    public function getCp(): string
    {
        return $this->cp;
    }

    /**
     * @param string $cp
     * @return User
     */
    public function setCp(string $cp): User
    {
        $this->cp = $cp;

        return $this;
    }

    /**
     * @return string
     */
    public function getTlf(): string
    {
        return $this->tlf;
    }

    /**
     * @param string $tlf
     * @return User
     */
    public function setTlf(string $tlf): User
    {
        $this->tlf = $tlf;

        return $this;
    }

    /**
     * @return string
     */
    public function getFax(): string
    {
        return $this->fax;
    }

    /**
     * @param string $fax
     * @return User
     */
    public function setFax(string $fax): User
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * @return string
     */
    public function getCif(): string
    {
        return $this->cif;
    }

    /**
     * @param string $cif
     * @return User
     */
    public function setCif(string $cif): User
    {
        $this->cif = $cif;

        return $this;
    }

    /**
     * @return string
     */
    public function getContacto(): string
    {
        return $this->contacto;
    }

    /**
     * @param string $contacto
     * @return User
     */
    public function setContacto(string $contacto): User
    {
        $this->contacto = $contacto;

        return $this;
    }

    /**
     * @return string
     */
    public function getCargo(): string
    {
        return $this->cargo;
    }

    /**
     * @param string $cargo
     * @return User
     */
    public function setCargo(string $cargo): User
    {
        $this->cargo = $cargo;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getFechaExp(): ?DateTime
    {
        return $this->fechaExp;
    }

    /**
     * @param DateTime $fechaExp
     * @return User
     */
    public function setFechaExp(DateTime $fechaExp = null): User
    {
        $this->fechaExp = $fechaExp;

        return $this;
    }

    /**
     * @return string
     */
    public function getUser(): ?string
    {
        return $this->user;
    }

    /**
     * @param string $user
     * @return User
     */
    public function setUser(string $user): User
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return string
     */
    public function getPsw(): string
    {
        return $this->psw;
    }

    /**
     * @param string $psw
     * @return User
     */
    public function setPsw(string $psw): User
    {
        $this->psw = $psw;

        return $this;
    }

    /**
     * @return float
     */
    public function getTarifa(): float
    {
        return $this->tarifa;
    }

    /**
     * @param float $tarifa
     * @return User
     */
    public function setTarifa(float $tarifa): User
    {
        $this->tarifa = $tarifa;

        return $this;
    }

    /**
     * @return int
     */
    public function getTipo(): int
    {
        return $this->tipo;
    }

    /**
     * @param int $tipo
     * @return User
     */
    public function setTipo(int $tipo): User
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * @return string
     */
    public function getObservaciones(): string
    {
        return $this->observaciones;
    }

    /**
     * @param string $observaciones
     * @return User
     */
    public function setObservaciones(string $observaciones): User
    {
        $this->observaciones = $observaciones;

        return $this;
    }

    /**
     * @return PreciosProductos[]|ArrayCollection
     */
    public function getProductos(): Collection
    {
        return $this->productos;
    }

    /**
     * @param PreciosProductos[]|Collection $productos
     * @return User
     */
    public function setProductos(Collection $productos): User
    {
        $this->productos = $productos;

        return $this;
    }

    /**
     * @return PreciosZonas[]|ArrayCollection
     */
    public function getZonas(): Collection
    {
        return $this->zonas;
    }

    /**
     * @param PreciosZonas[]|Collection $zonas
     * @return User
     */
    public function setZonas(Collection $zonas): User
    {
        $this->zonas = $zonas;

        return $this;
    }

    /**
     * @return PreciosZonas[]|ArrayCollection
     */
    public function getZonasActivas(): Collection
    {
        $zonasActivas = new ArrayCollection();

        foreach ($this->zonas as $zona) {
            if (!$zona->getActivo()) {
                continue;
            }

            $zonasActivas->add($zona);
        }

        return $zonasActivas;
    }

    /**
     * @return Asociaciones
     */
    public function getAsociacion(): ?Asociaciones
    {
        return $this->asociacion;
    }

    /**
     * @param Asociaciones $asociacion
     * @return User
     */
    public function setAsociacion(Asociaciones $asociacion = null): User
    {
        $this->asociacion = $asociacion;

        return $this;
    }

    /**
     * @return MargenesProds[]|ArrayCollection
     */
    public function getMargenes()
    {
        return $this->margenes;
    }

    /**
     * @param MargenesProds[]|ArrayCollection $margenes
     * @return User
     */
    public function setMargenes($margenes): User
    {
        $this->margenes = $margenes;

        return $this;
    }

    /**
     * @return Ips[]|ArrayCollection
     */
    public function getIps()
    {
        return $this->ips;
    }

    /**
     * @param Ips[]|ArrayCollection $ips
     * @return User
     */
    public function setIps($ips): User
    {
        $this->ips = $ips;

        return $this;
    }

    /**
     * @return Access[]|ArrayCollection
     */
    public function getAccesos()
    {
        return $this->accesos;
    }

    /**
     * @param Access[]|ArrayCollection $accesos
     * @return User
     */
    public function setAccesos($accesos): User
    {
        $this->accesos = $accesos;

        return $this;
    }

    /**
     * @return Formularios[]|ArrayCollection
     */
    public function getServicios()
    {
        return $this->servicios;
    }

    /**
     * @param Formularios[]|ArrayCollection $servicios
     * @return User
     */
    public function setServicios($servicios)
    {
        $this->servicios = $servicios;

        return $this;
    }

    /**
     * @return Formularios[]|ArrayCollection
     */
    public function getFormularios()
    {
        return $this->formularios;
    }

    /**
     * @param Formularios[]|ArrayCollection $formularios
     * @return User
     */
    public function setFormularios($formularios)
    {
        $this->formularios = $formularios;

        return $this;
    }
}
