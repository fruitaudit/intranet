<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * PreciosSupermercado
 *
 * @ORM\Table(name="precios_supermercado")
 * @ORM\Entity
 */
class PreciosSupermercado
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=250, nullable=false)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="img", type="string", nullable=true)
     */
    private $img;

    /**
     * @Assert\Image(maxSize = "3500k")
     */
    private $uploadedFile;

    /**
     * @var PreciosZonas
     *
     * @ORM\ManyToOne(targetEntity="PreciosZonas", inversedBy="supermercados")
     */
    private $zona;

    /**
     * @var PreciosActProd[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="PreciosActProd", mappedBy="supermercado")
     */
    private $precios;

    /**
     * @var Promociones[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Promociones", mappedBy="supermercado")
     */
    private $promociones;


    public function __construct()
    {
        $this->nombre = '';
        $this->precios = new ArrayCollection();
        $this->promociones = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNombre(): string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return PreciosSupermercado
     */
    public function setNombre(string $nombre): PreciosSupermercado
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getImg(): ?string
    {
        return $this->img;
    }

    /**
     * @param string|null $img
     * @return PreciosSupermercado
     */
    public function setImg(?string $img): PreciosSupermercado
    {
        $this->img = $img;

        return $this;
    }

    /**
     * @return UploadedFile|null
     */
    public function getUploadedFile(): ?UploadedFile
    {
        return $this->uploadedFile;
    }

    /**
     * @param mixed $uploadedFile
     * @return PreciosSupermercado
     */
    public function setUploadedFile($uploadedFile): PreciosSupermercado
    {
        $this->uploadedFile = $uploadedFile;

        return $this;
    }

    /**
     * @return PreciosZonas
     */
    public function getZona(): ?PreciosZonas
    {
        return $this->zona;
    }

    /**
     * @param PreciosZonas $zona
     * @return PreciosSupermercado
     */
    public function setZona(PreciosZonas $zona): PreciosSupermercado
    {
        $this->zona = $zona;

        return $this;
    }

    /**
     * @return PreciosActProd[]|ArrayCollection
     */
    public function getPrecios()
    {
        return $this->precios;
    }

    /**
     * @param PreciosActProd[]|ArrayCollection $precios
     * @return PreciosSupermercado
     */
    public function setPrecios($precios)
    {
        $this->precios = $precios;

        return $this;
    }

    /**
     * @return Promociones[]|ArrayCollection
     */
    public function getPromociones()
    {
        return $this->promociones;
    }

    /**
     * @param Promociones[]|ArrayCollection $promociones
     * @return PreciosSupermercado
     */
    public function setPromociones($promociones)
    {
        $this->promociones = $promociones;

        return $this;
    }

    public function __toString()
    {
        return $this->getNombre();
    }
}

