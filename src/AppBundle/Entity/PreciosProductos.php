<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * PreciosProductos
 *
 * @ORM\Table(name="precios_productos")
 * @ORM\Entity
 */
class PreciosProductos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="foro", type="integer", nullable=true)
     */
    private $foro;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=250, nullable=true)
     */
    private $nombre;

    /**
     * @var PreciosActProd[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="PreciosActProd", mappedBy="producto")
     */
    private $precios;

    /**
     * @var MargenesProds[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="MargenesProds", mappedBy="producto")
     */
    private $margenes;

    /**
     * @var Promociones[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Promociones", mappedBy="producto")
     */
    private $promociones;


    public function __construct()
    {
        $this->nombre = '';
        $this->foro = 0;
        $this->precios = new ArrayCollection();
        $this->margenes = new ArrayCollection();
        $this->promociones = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getForo(): int
    {
        return $this->foro;
    }

    /**
     * @param int $foro
     * @return PreciosProductos
     */
    public function setForo(int $foro): PreciosProductos
    {
        $this->foro = $foro;

        return $this;
    }

    /**
     * @return string
     */
    public function getNombre(): string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return PreciosProductos
     */
    public function setNombre(string $nombre): PreciosProductos
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function __toString()
    {
        return $this->getNombre();
    }

    /**
     * @return PreciosActProd[]|ArrayCollection
     */
    public function getPrecios()
    {
        return $this->precios;
    }

    /**
     * @param PreciosActProd[]|ArrayCollection $precios
     * @return PreciosProductos
     */
    public function setPrecios($precios)
    {
        $this->precios = $precios;

        return $this;
    }

    /**
     * @return MargenesProds[]|ArrayCollection
     */
    public function getMargenes()
    {
        return $this->margenes;
    }

    /**
     * @param MargenesProds[]|ArrayCollection $margenes
     * @return PreciosProductos
     */
    public function setMargenes($margenes)
    {
        $this->margenes = $margenes;

        return $this;
    }
}

