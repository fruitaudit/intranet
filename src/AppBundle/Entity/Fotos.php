<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Fotos
 *
 * @ORM\Table(name="fotos")
 * @ORM\Entity
 */
class Fotos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="formulario", type="integer", nullable=false)
     */
    private $formulario;

    /**
     * @var integer
     *
     * @ORM\Column(name="perito", type="integer", nullable=false)
     */
    private $perito;

    /**
     * @var string
     *
     * @ORM\Column(name="file", type="string", length=250, nullable=false)
     */
    private $file;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


}

