<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * EstadoFormulario
 *
 * @ORM\Table(name="estado_formulario")
 * @ORM\Entity
 */
class EstadoFormulario
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $nombre;

    /**
     * @var Formularios[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Formularios", mappedBy="estado")
     */
    private $formularios;


    public function __construct()
    {
        $this->formularios = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return EstadoFormulario
     */
    public function setNombre(string $nombre): EstadoFormulario
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function __toString()
    {
        return $this->getNombre().'';
    }
}

