<?php

namespace AppBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Promociones
 *
 * @ORM\Table(name="promociones")
 * @ORM\Entity
 */
class Promociones
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var PreciosProductos
     *
     * @ORM\ManyToOne(targetEntity="PreciosProductos", inversedBy="promociones")
     */
    private $producto;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=250, nullable=false)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion", type="text", length=65535, nullable=true)
     */
    private $descripcion;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="fecha", type="date", nullable=false)
     */
    private $fecha;

    /**
     * @var string|null
     *
     * @ORM\Column(name="precio", type="string", length=100, nullable=false)
     */
    private $precio;

    /**
     * @var PreciosSupermercado|null
     *
     * @ORM\ManyToOne(targetEntity="PreciosSupermercado", inversedBy="promociones")
     */
    private $supermercado;

    /**
     * @var string|null
     *
     * @ORM\Column(name="envase", type="string", length=100, nullable=true)
     */
    private $envase;

    /**
     * @var integer|null
     *
     * @ORM\Column(name="semana", type="integer", nullable=true)
     */
    private $semana;

    /**
     * @var PreciosZonas|null
     *
     * @ORM\ManyToOne(targetEntity="PreciosZonas", inversedBy="promociones")
     */
    private $zona;

    /**
     * @var string|null
     *
     * @ORM\Column(name="origen", type="string", length=250, nullable=true)
     */
    private $origen;

    /**
     * @var bool
     *
     * @ORM\Column(name="activo", type="boolean", nullable=false)
     */
    private $activo;


    public function __construct()
    {
        $this->activo = 1;
        $this->fecha = new DateTime();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return PreciosProductos
     */
    public function getProducto(): ?PreciosProductos
    {
        return $this->producto;
    }

    /**
     * @param PreciosProductos $producto
     * @return Promociones
     */
    public function setProducto(PreciosProductos $producto): Promociones
    {
        $this->producto = $producto;

        return $this;
    }

    /**
     * @return string
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return Promociones
     */
    public function setNombre(string $nombre): Promociones
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    /**
     * @param string|null $descripcion
     * @return Promociones
     */
    public function setDescripcion(?string $descripcion): Promociones
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getFecha(): DateTime
    {
        return $this->fecha;
    }

    /**
     * @param DateTime $fecha
     * @return Promociones
     */
    public function setFecha(DateTime $fecha): Promociones
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPrecio(): ?string
    {
        return $this->precio;
    }

    /**
     * @param string|null $precio
     * @return Promociones
     */
    public function setPrecio(?string $precio): Promociones
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * @return PreciosSupermercado|null
     */
    public function getSupermercado(): ?PreciosSupermercado
    {
        return $this->supermercado;
    }

    /**
     * @param PreciosSupermercado|null $supermercado
     * @return Promociones
     */
    public function setSupermercado(?PreciosSupermercado $supermercado): Promociones
    {
        $this->supermercado = $supermercado;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEnvase(): ?string
    {
        return $this->envase;
    }

    /**
     * @param string|null $envase
     * @return Promociones
     */
    public function setEnvase(?string $envase): Promociones
    {
        $this->envase = $envase;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getSemana(): ?int
    {
        return $this->semana;
    }

    /**
     * @param int|null $semana
     * @return Promociones
     */
    public function setSemana(?int $semana): Promociones
    {
        $this->semana = $semana;

        return $this;
    }

    /**
     * @return PreciosZonas|null
     */
    public function getZona(): ?PreciosZonas
    {
        return $this->zona;
    }

    /**
     * @param PreciosZonas|null $zona
     * @return Promociones
     */
    public function setZona(?PreciosZonas $zona): Promociones
    {
        $this->zona = $zona;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getOrigen(): ?string
    {
        return $this->origen;
    }

    /**
     * @param string|null $origen
     * @return Promociones
     */
    public function setOrigen(?string $origen): Promociones
    {
        $this->origen = $origen;

        return $this;
    }

    /**
     * @return bool
     */
    public function isActivo(): bool
    {
        return $this->activo;
    }

    /**
     * @param bool $activo
     * @return Promociones
     */
    public function setActivo(bool $activo): Promociones
    {
        $this->activo = $activo;

        return $this;
    }
}

