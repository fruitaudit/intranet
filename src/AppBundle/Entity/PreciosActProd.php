<?php

namespace AppBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * PreciosActProd
 *
 * @ORM\Table(name="precios_act_prod")
 * @ORM\Entity
 */
class PreciosActProd
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var PreciosProductos
     *
     * @ORM\ManyToOne(targetEntity="PreciosProductos", inversedBy="precios")
     */
    private $producto;

    /**
     * @var string
     *
     * @ORM\Column(name="envase", type="string", length=100, nullable=false)
     */
    private $envase;

    /**
     * @var string
     *
     * @ORM\Column(name="origen", type="string", length=100, nullable=false)
     */
    private $origen;

    /**
     * @var float
     *
     * @ORM\Column(name="precio", type="float", precision=10, scale=0, nullable=false)
     */
    private $precio;

    /**
     * @var PreciosZonas
     *
     * @ORM\ManyToOne(targetEntity="PreciosZonas", inversedBy="precios")
     */
    private $zona;

    /**
     * @var PreciosSupermercado
     *
     * @ORM\ManyToOne(targetEntity="PreciosSupermercado", inversedBy="precios")
     */
    private $supermercado;

    /**
     * @var bool
     *
     * @ORM\Column(name="estado", type="boolean", nullable=false)
     */
    private $estado;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="fecha", type="date", nullable=true)
     */
    private $fecha;

    /**
     * @var string|null
     *
     * @ORM\Column(name="info", type="string", length=250, nullable=true)
     */
    private $info;


    public function __construct()
    {
        $this->estado = 1;
        $this->fecha = new DateTime();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return PreciosProductos
     */
    public function getProducto(): ?PreciosProductos
    {
        return $this->producto;
    }

    /**
     * @param PreciosProductos $producto
     * @return PreciosActProd
     */
    public function setProducto(PreciosProductos $producto): PreciosActProd
    {
        $this->producto = $producto;

        return $this;
    }

    /**
     * @return string
     */
    public function getEnvase(): ?string
    {
        return $this->envase;
    }

    /**
     * @param string $envase
     * @return PreciosActProd
     */
    public function setEnvase(string $envase): PreciosActProd
    {
        $this->envase = $envase;

        return $this;
    }

    /**
     * @return string
     */
    public function getOrigen(): ?string
    {
        return $this->origen;
    }

    /**
     * @param string $origen
     * @return PreciosActProd
     */
    public function setOrigen(string $origen): PreciosActProd
    {
        $this->origen = $origen;

        return $this;
    }

    /**
     * @return float
     */
    public function getPrecio(): ?float
    {
        return $this->precio;
    }

    /**
     * @param float $precio
     * @return PreciosActProd
     */
    public function setPrecio(float $precio): PreciosActProd
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * @return PreciosZonas
     */
    public function getZona(): ?PreciosZonas
    {
        return $this->zona;
    }

    /**
     * @param PreciosZonas $zona
     * @return PreciosActProd
     */
    public function setZona(PreciosZonas $zona): PreciosActProd
    {
        $this->zona = $zona;

        return $this;
    }

    /**
     * @return PreciosSupermercado
     */
    public function getSupermercado(): ?PreciosSupermercado
    {
        return $this->supermercado;
    }

    /**
     * @param PreciosSupermercado $supermercado
     * @return PreciosActProd
     */
    public function setSupermercado(PreciosSupermercado $supermercado): PreciosActProd
    {
        $this->supermercado = $supermercado;

        return $this;
    }

    /**
     * @return bool
     */
    public function getEstado(): bool
    {
        return $this->estado;
    }

    /**
     * @param bool $estado
     * @return PreciosActProd
     */
    public function setEstado(bool $estado): PreciosActProd
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getFecha(): DateTime
    {
        return $this->fecha;
    }

    /**
     * @param DateTime $fecha
     * @return PreciosActProd
     */
    public function setFecha(DateTime $fecha): PreciosActProd
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getInfo(): ?string
    {
        return $this->info;
    }

    /**
     * @param string $info
     * @return PreciosActProd
     */
    public function setInfo(string $info = null): PreciosActProd
    {
        $this->info = $info;

        return $this;
    }
}