<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MargenesProds
 *
 * @ORM\Table(name="margenes_prods")
 * @ORM\Entity
 */
class MargenesProds
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="margenes")
     */
    private $user;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="date", nullable=false)
     */
    private $fecha;

    /**
     * @var float
     *
     * @ORM\Column(name="precio", type="float", precision=10, scale=0, nullable=false)
     */
    private $precio;

    /**
     * @var PreciosProductos
     *
     * @ORM\ManyToOne(targetEntity="PreciosProductos", inversedBy="margenes")
     */
    private $producto;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return MargenesProds
     */
    public function setUser(User $user): MargenesProds
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getFecha(): \DateTime
    {
        return $this->fecha;
    }

    /**
     * @param \DateTime $fecha
     * @return MargenesProds
     */
    public function setFecha(\DateTime $fecha): MargenesProds
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * @return float
     */
    public function getPrecio(): float
    {
        return $this->precio;
    }

    /**
     * @param float $precio
     * @return MargenesProds
     */
    public function setPrecio(float $precio): MargenesProds
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * @return PreciosProductos
     */
    public function getProducto(): PreciosProductos
    {
        return $this->producto;
    }

    /**
     * @param PreciosProductos $producto
     * @return MargenesProds
     */
    public function setProducto(PreciosProductos $producto): MargenesProds
    {
        $this->producto = $producto;

        return $this;
    }
}

