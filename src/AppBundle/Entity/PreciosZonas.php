<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * PreciosZonas
 *
 * @ORM\Table(name="precios_zonas")
 * @ORM\Entity
 */
class PreciosZonas
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=250, nullable=true)
     */
    private $nombre;

    /**
     * @var bool
     *
     * @ORM\Column(name="activo", type="boolean", nullable=false)
     */
    private $activo;

    /**
     * @var PreciosSupermercado[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="PreciosSupermercado", mappedBy="zona")
     */
    private $supermercados;

    /**
     * @var PreciosActProd[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="PreciosActProd", mappedBy="zona")
     */
    private $precios;

    /**
     * @var Directorio[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Directorio", mappedBy="zona")
     */
    private $directorios;

    /**
     * @var Promociones[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Promociones", mappedBy="zona")
     */
    private $promociones;


    public function __construct()
    {
        $this->nombre = '';
        $this->activo = true;
        $this->supermercados = new ArrayCollection();
        $this->precios = new ArrayCollection();
        $this->directorios = new ArrayCollection();
        $this->promociones = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNombre(): string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return PreciosZonas
     */
    public function setNombre(string $nombre): PreciosZonas
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * @return bool
     */
    public function getActivo(): bool
    {
        return $this->activo;
    }

    /**
     * @param bool $activo
     * @return PreciosZonas
     */
    public function setActivo(bool $activo): PreciosZonas
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * @return PreciosSupermercado[]|ArrayCollection
     */
    public function getSupermercados()
    {
        return $this->supermercados;
    }

    /**
     * @param PreciosSupermercado[]|ArrayCollection $supermercados
     * @return PreciosZonas
     */
    public function setSupermercados($supermercados): PreciosZonas
    {
        $this->supermercados = $supermercados;

        return $this;
    }

    public function __toString()
    {
        return $this->getNombre();
    }

    /**
     * @return PreciosActProd[]|ArrayCollection
     */
    public function getPrecios()
    {
        return $this->precios;
    }

    /**
     * @param PreciosActProd[]|ArrayCollection $precios
     * @return PreciosZonas
     */
    public function setPrecios($precios)
    {
        $this->precios = $precios;

        return $this;
    }

    /**
     * @return Directorio[]|ArrayCollection
     */
    public function getDirectorios()
    {
        return $this->directorios;
    }

    /**
     * @param Directorio[]|ArrayCollection $directorios
     * @return PreciosZonas
     */
    public function setDirectorios($directorios)
    {
        $this->directorios = $directorios;

        return $this;
    }

    /**
     * @return Promociones[]|ArrayCollection
     */
    public function getPromociones()
    {
        return $this->promociones;
    }

    /**
     * @param Promociones[]|ArrayCollection $promociones
     * @return PreciosZonas
     */
    public function setPromociones($promociones)
    {
        $this->promociones = $promociones;

        return $this;
    }
}

