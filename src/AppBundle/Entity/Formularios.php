<?php

namespace AppBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Formularios
 *
 * @ORM\Table(name="formularios")
 * @ORM\Entity
 */
class Formularios
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="fecha", type="date", nullable=false)
     */
    private $fecha;

    /**
     * @var User|null
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="servicios")
     */
    private $cliente;

    /**
     * @var User|null
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="formularios")
     */
    private $perito;

    /**
     * @var EstadoFormulario|null
     *
     * @ORM\ManyToOne(targetEntity="EstadoFormulario", inversedBy="formularios")
     */
    private $estado;

    /**
     * @var TipoFormulario|null
     *
     * @ORM\ManyToOne(targetEntity="TipoFormulario", inversedBy="formularios")
     */
    private $tipo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="campo0", type="text", nullable=false)
     */
    private $lugarMercancia;

    /**
     * @var string|null
     *
     * @ORM\Column(name="campo1", type="text", nullable=false)
     */
    private $fechaHorario;

    /**
     * @var string|null
     *
     * @ORM\Column(name="campo2", type="text", nullable=false)
     */
    private $exportadorEnvasador;

    /**
     * @var string|null
     *
     * @ORM\Column(name="campo3", type="text", nullable=false)
     */
    private $destinatario;

    /**
     * @var string|null
     *
     * @ORM\Column(name="campo4", type="text", nullable=false)
     */
    private $descripcionMercaciaEtiquetado;

    /**
     * @var string|null
     *
     * @ORM\Column(name="campo5", type="text", nullable=false)
     */
    private $embalaje;

    /**
     * @var string|null
     *
     * @ORM\Column(name="campo6", type="text", nullable=false)
     */
    private $empaquetadoEnvio;

    /**
     * @var string|null
     *
     * @ORM\Column(name="campo7", type="text", nullable=false)
     */
    private $condicionesEntrega;

    /**
     * @var string|null
     *
     * @ORM\Column(name="campo8", type="text", nullable=false)
     */
    private $transporte;

    /**
     * @var string|null
     *
     * @ORM\Column(name="campo9", type="text", nullable=false)
     */
    private $lugarFechaDescarga;

    /**
     * @var string|null
     *
     * @ORM\Column(name="campo10", type="text", nullable=false)
     */
    private $especificacionesTecnicas;

    /**
     * @var string|null
     *
     * @ORM\Column(name="campo11", type="text", nullable=false)
     */
    private $motivos;

    /**
     * @var string|null
     *
     * @ORM\Column(name="campo12", type="text", nullable=false)
     */
    private $contactoOrigenDestino;

    /**
     * @var string|null
     *
     * @ORM\Column(name="campo13", type="text", nullable=false)
     */
    private $albaranSalida;

    /**
     * @var string|null
     *
     * @ORM\Column(name="campo14", type="text", nullable=false)
     */
    private $otraInformacion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="file_doc", type="string", nullable=true)
     */
    private $fileDoc;

    /**
     * @var string|null
     *
     * @ORM\Column(name="file_xls", type="string", nullable=true)
     */
    private $fileXls;


    public function __construct()
    {
        $this->fecha = new DateTime();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return DateTime
     */
    public function getFecha(): DateTime
    {
        return $this->fecha;
    }

    /**
     * @param DateTime $fecha
     * @return Formularios
     */
    public function setFecha(DateTime $fecha): Formularios
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getCliente(): ?User
    {
        return $this->cliente;
    }

    /**
     * @param User|null $cliente
     * @return Formularios
     */
    public function setCliente(?User $cliente): Formularios
    {
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getPerito(): ?User
    {
        return $this->perito;
    }

    /**
     * @param User|null $perito
     * @return Formularios
     */
    public function setPerito(?User $perito): Formularios
    {
        $this->perito = $perito;

        return $this;
    }

    /**
     * @return EstadoFormulario|null
     */
    public function getEstado(): ?EstadoFormulario
    {
        return $this->estado;
    }

    /**
     * @param EstadoFormulario|null $estado
     * @return Formularios
     */
    public function setEstado(?EstadoFormulario $estado): Formularios
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * @return TipoFormulario|null
     */
    public function getTipo(): ?TipoFormulario
    {
        return $this->tipo;
    }

    /**
     * @param TipoFormulario|null $tipo
     * @return Formularios
     */
    public function setTipo(?TipoFormulario $tipo): Formularios
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLugarMercancia(): ?string
    {
        return $this->lugarMercancia;
    }

    /**
     * @param string|null $lugarMercancia
     * @return Formularios
     */
    public function setLugarMercancia(?string $lugarMercancia): Formularios
    {
        $this->lugarMercancia = $lugarMercancia;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFechaHorario(): ?string
    {
        return $this->fechaHorario;
    }

    /**
     * @param string|null $fechaHorario
     * @return Formularios
     */
    public function setFechaHorario(?string $fechaHorario): Formularios
    {
        $this->fechaHorario = $fechaHorario;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getExportadorEnvasador(): ?string
    {
        return $this->exportadorEnvasador;
    }

    /**
     * @param string|null $exportadorEnvasador
     * @return Formularios
     */
    public function setExportadorEnvasador(?string $exportadorEnvasador): Formularios
    {
        $this->exportadorEnvasador = $exportadorEnvasador;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDestinatario(): ?string
    {
        return $this->destinatario;
    }

    /**
     * @param string|null $destinatario
     * @return Formularios
     */
    public function setDestinatario(?string $destinatario): Formularios
    {
        $this->destinatario = $destinatario;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescripcionMercaciaEtiquetado(): ?string
    {
        return $this->descripcionMercaciaEtiquetado;
    }

    /**
     * @param string|null $descripcionMercaciaEtiquetado
     * @return Formularios
     */
    public function setDescripcionMercaciaEtiquetado(?string $descripcionMercaciaEtiquetado): Formularios
    {
        $this->descripcionMercaciaEtiquetado = $descripcionMercaciaEtiquetado;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmbalaje(): ?string
    {
        return $this->embalaje;
    }

    /**
     * @param string|null $embalaje
     * @return Formularios
     */
    public function setEmbalaje(?string $embalaje): Formularios
    {
        $this->embalaje = $embalaje;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmpaquetadoEnvio(): ?string
    {
        return $this->empaquetadoEnvio;
    }

    /**
     * @param string|null $empaquetadoEnvio
     * @return Formularios
     */
    public function setEmpaquetadoEnvio(?string $empaquetadoEnvio): Formularios
    {
        $this->empaquetadoEnvio = $empaquetadoEnvio;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCondicionesEntrega(): ?string
    {
        return $this->condicionesEntrega;
    }

    /**
     * @param string|null $condicionesEntrega
     * @return Formularios
     */
    public function setCondicionesEntrega(?string $condicionesEntrega): Formularios
    {
        $this->condicionesEntrega = $condicionesEntrega;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTransporte(): ?string
    {
        return $this->transporte;
    }

    /**
     * @param string|null $transporte
     * @return Formularios
     */
    public function setTransporte(?string $transporte): Formularios
    {
        $this->transporte = $transporte;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLugarFechaDescarga(): ?string
    {
        return $this->lugarFechaDescarga;
    }

    /**
     * @param string|null $lugarFechaDescarga
     * @return Formularios
     */
    public function setLugarFechaDescarga(?string $lugarFechaDescarga): Formularios
    {
        $this->lugarFechaDescarga = $lugarFechaDescarga;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEspecificacionesTecnicas(): ?string
    {
        return $this->especificacionesTecnicas;
    }

    /**
     * @param string|null $especificacionesTecnicas
     * @return Formularios
     */
    public function setEspecificacionesTecnicas(?string $especificacionesTecnicas): Formularios
    {
        $this->especificacionesTecnicas = $especificacionesTecnicas;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMotivos(): ?string
    {
        return $this->motivos;
    }

    /**
     * @param string|null $motivos
     * @return Formularios
     */
    public function setMotivos(?string $motivos): Formularios
    {
        $this->motivos = $motivos;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getContactoOrigenDestino(): ?string
    {
        return $this->contactoOrigenDestino;
    }

    /**
     * @param string|null $contactoOrigenDestino
     * @return Formularios
     */
    public function setContactoOrigenDestino(?string $contactoOrigenDestino): Formularios
    {
        $this->contactoOrigenDestino = $contactoOrigenDestino;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAlbaranSalida(): ?string
    {
        return $this->albaranSalida;
    }

    /**
     * @param string|null $albaranSalida
     * @return Formularios
     */
    public function setAlbaranSalida(?string $albaranSalida): Formularios
    {
        $this->albaranSalida = $albaranSalida;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getOtraInformacion(): ?string
    {
        return $this->otraInformacion;
    }

    /**
     * @param string|null $otraInformacion
     * @return Formularios
     */
    public function setOtraInformacion(?string $otraInformacion): Formularios
    {
        $this->otraInformacion = $otraInformacion;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFileDoc(): ?string
    {
        return $this->fileDoc;
    }

    /**
     * @param string|null $fileDoc
     * @return Formularios
     */
    public function setFileDoc(?string $fileDoc): Formularios
    {
        $this->fileDoc = $fileDoc;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFileXls(): ?string
    {
        return $this->fileXls;
    }

    /**
     * @param string|null $fileXls
     * @return Formularios
     */
    public function setFileXls(?string $fileXls): Formularios
    {
        $this->fileXls = $fileXls;

        return $this;
    }
}

