<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Noticias
 *
 * @ORM\Table(name="noticias")
 * @ORM\Entity
 */
class Noticias
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="date", nullable=false)
     */
    private $fecha;

    /**
     * @var integer
     *
     * @ORM\Column(name="activo", type="integer", nullable=false)
     */
    private $activo = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=250, nullable=true)
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo_eng", type="string", length=250, nullable=true)
     */
    private $tituloEng = '';

    /**
     * @var string
     *
     * @ORM\Column(name="titulo_fra", type="string", length=250, nullable=true)
     */
    private $tituloFra = '';

    /**
     * @var string
     *
     * @ORM\Column(name="cuerpo", type="text", length=65535, nullable=true)
     */
    private $cuerpo;

    /**
     * @var string
     *
     * @ORM\Column(name="cuerpo_eng", type="text", length=65535, nullable=true)
     */
    private $cuerpoEng;

    /**
     * @var string
     *
     * @ORM\Column(name="cuerpo_fra", type="text", length=65535, nullable=true)
     */
    private $cuerpoFra;

    /**
     * @var string
     *
     * @ORM\Column(name="img", type="string", length=100, nullable=true)
     */
    private $img;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo_ale", type="string", length=250, nullable=true)
     */
    private $tituloAle;

    /**
     * @var string
     *
     * @ORM\Column(name="cuerpo_ale", type="text", length=65535, nullable=true)
     */
    private $cuerpoAle;

    /**
     * @var integer
     *
     * @ORM\Column(name="tipo", type="integer", nullable=true)
     */
    private $tipo = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


}

