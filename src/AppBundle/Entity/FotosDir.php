<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FotosDir
 *
 * @ORM\Table(name="fotos_dir")
 * @ORM\Entity
 */
class FotosDir
{
    /**
     * @var string
     *
     * @ORM\Column(name="archivo", type="string", length=250, nullable=false)
     */
    private $archivo = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="id_dir", type="integer", nullable=false)
     */
    private $idDir = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


}

