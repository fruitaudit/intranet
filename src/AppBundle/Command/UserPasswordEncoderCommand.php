<?php


namespace AppBundle\Command;

use Exception;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Core\User\User;

/**
 * Class UserPasswordEncoderCommand
 * @package AppBundle\Command
 */
class UserPasswordEncoderCommand extends ContainerAwareCommand
{
    protected static $defaultName = 'app:encode-password';

    private $encoderFactory;
    private $userClasses;

    public function __construct(EncoderFactoryInterface $encoderFactory = null, array $userClasses = [])
    {
        if (null === $encoderFactory) {
            @trigger_error(
                sprintf(
                    'Passing null as the first argument of "%s()" is deprecated since Symfony 3.3 and support for it will be removed in 4.0. If the command was registered by convention, make it a service instead.',
                    __METHOD__
                ),
                E_USER_DEPRECATED
            );
        }

        $this->encoderFactory = $encoderFactory;
        $this->userClasses = $userClasses;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setDescription('Encodes a password.')
            ->addArgument(
                'user-class',
                InputArgument::OPTIONAL,
                'The User entity class path associated with the encoder used to encode the password.'
            )
            ->setHelp(
                <<<EOF

The <info>%command.name%</info> command encodes passwords according to your
security configuration. This command is mainly used to generate passwords for
the <comment>in_memory</comment> user provider type and for changing passwords
in the database while developing the application.

Suppose that you have the following security configuration in your application:

<comment>
# app/config/security.yml
security:
    encoders:
        Symfony\Component\Security\Core\User\User: plaintext
        App\Entity\User: bcrypt
</comment>

If you execute the command non-interactively, the first available configured
user class under the <comment>security.encoders</comment> key is used:

  <info>php %command.full_name% --no-interaction</info>

Pass the full user class path as the argument to encode passwords for
your own entities:

  <info>php %command.full_name% --no-interaction App\Entity\User</info>

EOF
            );
    }

    /**
     * {@inheritdoc}
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $errorIo = $output instanceof ConsoleOutputInterface ? new SymfonyStyle(
            $input, $output->getErrorOutput()
        ) : $io;

        $input->isInteractive() ? $errorIo->title('Symfony Password Encoder Utility') : $errorIo->newLine();

        $userClass = $this->getUserClass($input, $io);
        $encoderFactory = $this->encoderFactory ?: $this->getContainer()->get('security.encoder_factory');
        $encoder = $encoderFactory->getEncoder($userClass);

        $entityManager = $this->getContainer()->get('doctrine')->getManager();

        $users = $entityManager->getRepository($userClass)->findAll();

        foreach ($users as $user) {
            $salt = $user->getSalt();
            if (!$salt) {
                $salt = $this->generateSalt();
            }

            $encodedPassword = $encoder->encodePassword($user->getPassword(), $salt);

            $user->setPassword($encodedPassword);
            $entityManager->persist($user);
        }

        $entityManager->flush();
    }

    /**
     * @return string
     * @throws Exception
     */
    private function generateSalt(): string
    {
        return base64_encode(random_bytes(30));
    }

    /**
     * @param InputInterface $input
     * @param SymfonyStyle $io
     * @return mixed|string|string[]|null
     */
    private function getUserClass(InputInterface $input, SymfonyStyle $io)
    {
        if (null !== $userClass = $input->getArgument('user-class')) {
            return $userClass;
        }

        if (empty($this->userClasses)) {
            if (null === $this->encoderFactory) {
                // BC to be removed and simply keep the exception whenever there is no configured user classes in 4.0
                return User::class;
            }

            throw new RuntimeException('There are no configured encoders for the "security" extension.');
        }

        if (!$input->isInteractive() || 1 === count($this->userClasses)) {
            return reset($this->userClasses);
        }

        $userClasses = $this->userClasses;
        natcasesort($userClasses);
        $userClasses = array_values($userClasses);

        return $io->choice(
            'For which user class would you like to encode a password?',
            $userClasses,
            reset($userClasses)
        );
    }
}
